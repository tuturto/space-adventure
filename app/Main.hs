{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE OverloadedStrings #-}
module Main (main) where

import qualified Data.Attoparsec.Text as A
import qualified Data.Text as T
import SDL
import Import
import Foreign.C.Types ( CInt(..), CFloat(..) )
import Run
  ( runApplication
  , loadResources
  , freeResources
  )
import Options.Applicative.Simple
import RIO.List

import qualified Paths_space_adventure

-- |Main function. Parses command line parameters and starts
-- execution of the program.
main :: IO ()
main = do
  (temp, ()) <- simpleOptions
    $(simpleVersion Paths_space_adventure.version)
    "Space Adventure"
    ("Space adventure is a computer game set in the far reaches of space." <>
     "Player is a lone, sentient AI, who is on a mission to find a way " <>
     "back to their home planet. Trade, explore and discover secrets of the space." 
    )
    (TempOptions
      <$> switch 
            ( long "verbose"
              <> short 'v'
              <> help "Verbose output"
            )
      <*> switch 
            ( long "development"
            <> short 'd'
            <> help "Development mode"
            )
      <*> option resolutionReader
            ( long "resolution"
            <> short 'r'
            <> help "Screen resolution"
            <> showDefaultWith prettyResolution
            <> value (V2 1920 1080)
            )
      <*> switch
            ( long "windowed"
              <> short 'w'
              <> help "Windowed mode"
            )
    )
    empty
  let
    options = Options (tempOptionsVerbose temp)
                      (tempOptionsDevelopment temp)
                      (tempOptionsResolution temp)
                      (tempOptionsWindowed temp)
                      (renderScale $ tempOptionsResolution temp)
                      (viewport $ tempOptionsResolution temp)

  bracket (loadResources options)
          freeResources
          (runApplication options)


data TempOptions = TempOptions
  { tempOptionsVerbose     :: !Bool
  , tempOptionsDevelopment :: !Bool
  , tempOptionsResolution  :: !(V2 CInt)
  , tempOptionsWindowed    :: !Bool
  }


attoparsecReader :: A.Parser a -> ReadM a
attoparsecReader p = eitherReader (A.parseOnly p . T.pack)


resolutionReader :: ReadM (V2 CInt)
resolutionReader =
  attoparsecReader resolutionParser


resolutionParser :: A.Parser (V2 CInt)
resolutionParser = do
  x <- A.decimal
  _ <- A.char 'x'
  y <- A.decimal
  return $ V2 x y


prettyResolution :: V2 CInt -> String
prettyResolution (V2 x y) =
  (show x) ++ "x" ++ (show y)



renderScale :: V2 CInt -> V2 CFloat
renderScale (V2 w h) =
  V2 scale scale
  where
    scale = fromIntegral . fromMaybe 1 $ minimumMaybe [w `div` 640, h `div` 480]


viewport :: V2 CInt -> Rectangle CInt
viewport res@(V2 w h) =
  Rectangle (P (V2 (lx `div` 2) (ly `div` 2))) (V2 640 480)
  where
    -- find out render scaling
    (V2 rx _) = renderScale res
    scale = fromIntegral $ fromEnum rx :: CInt

    -- scale resolution by render scaling
    (sw, sh) = ( w `div` scale, h `div` scale)

    -- transpose by internal resolution
    (lx, ly) = (sw - 640, sh - 480)
