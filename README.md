# space-adventure

## Execute

* Run `stack exec -- space-adventure-exe` to start the game. Following switches are supported.
    * `--verbose` Display more logging
    * `--development` Enable development mode and run in a window

## Run tests

`stack test`

## Credits

Space adventure uses Pixel Simplicity GUI elements from https://opengameart.org/content/simple-pixel-gui-programmer-art
