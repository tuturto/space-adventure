{-|
Module      : System
Description : Module for representing state of the program.
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental

System module contains data types used to represent top level state of the
program.
-}
module System
  ( GameData(..)
  , Model(..)
  , _GameInMainMenu
  , _DockedInSpaceStation
  , _GameExiting
  , gameDataSystems
  , tick
  )
  where

import Import
import Control.Lens ( makeLenses, makePrisms )
import Data.Aeson.TH ( deriveJSON, defaultOptions )
import Model
import UI.MainMenu.Model ( MainMenuModel )
import UI.SpaceStation.Model ( SpaceStationModel )


-- |State of the game
data GameData = GameData
  { _gameDataSystems :: ![StarSystem]
  }
  deriving (Show, Read, Eq)


-- |Current state of the program
data Model = 
  GameInMainMenu Tick MainMenuModel
  | GameExiting Tick
  | DockedInSpaceStation Tick SpaceStationModel SpaceStationId GameData


tick :: Lens' Model Tick
tick =
  lens getTick setTick
  where
    getTick = \m -> 
      (case m of
        GameInMainMenu t _ ->
          t

        GameExiting t ->
          t

        DockedInSpaceStation t _ _ _ ->
          t)

    setTick = \m t ->
      (case m of
        GameInMainMenu _ mm ->
          GameInMainMenu t mm

        GameExiting _ ->
          GameExiting t

        DockedInSpaceStation _ sm sId gd ->
          DockedInSpaceStation t sm sId gd)



makePrisms ''Model
makeLenses ''GameData

deriveJSON defaultOptions ''GameData
