{-|
Module      : Common
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module Common
  ( Frequency(..)
  , chooseM
  , pick
  , safeHead
  , safeTail
  )
where

import RIO
import Control.Monad.Random.Strict

-- | Get head of a list, if list is empty, return Nothing
safeHead :: [a] -> Maybe a
safeHead (x:_) = Just x
safeHead _ = Nothing


-- | Get tail of a list, if list is empty, return empty list
safeTail :: [a] -> [a]
safeTail [] = []
safeTail (_:xs) = xs


-- | Randomly choose item from weighted list
-- | In case of empty list, Nothing is returned
chooseM :: RandomGen g => [Frequency a] -> Rand g (Maybe a)
chooseM [] =
  return Nothing

chooseM items = do
  let total = sum $ fmap (\(Frequency x _) -> x) items
  n <- getRandomR (1, total)
  return $ pick items n


-- | Frequency or weight of a
data Frequency a = Frequency Int a
  deriving (Show, Read, Eq)


-- | Helper function to pick item from weighted list
pick :: [Frequency a] -> Int -> Maybe a
pick [] _ = Nothing

pick (Frequency x item:xs) i
  | i <= x = Just item
  | otherwise = pick xs (i - x)
