{-|
Module      : Types
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module Types
  ( App(..)
  , AppConfig(..)
  , Options(..)
  , Tick(..)
  , unTick
  , appConfigStarLimit
  , appConfigGalaxySize
  , optionsVerbose
  , optionsDevelopment
  , optionsResolution
  , optionsRenderScale
  , optionsViewport
  , optionsWindowed
  ) where

import RIO
import RIO.Process ( HasProcessContext(..), ProcessContext )
import Control.Lens ( makeLenses )
import Foreign.C.Types ( CInt(..), CFloat(..) )
import SDL
import UI.Types ( Textures )

-- |Command line arguments
data Options = Options
  { _optionsVerbose     :: !Bool
  , _optionsDevelopment :: !Bool
  , _optionsResolution  :: !(V2 CInt)
  , _optionsWindowed    :: !Bool
  , _optionsRenderScale :: !(V2 CFloat)
  , _optionsViewport    :: !(Rectangle CInt)
  }
  deriving (Show, Read, Eq)


data App = App
  { appLogFunc        :: !LogFunc
  , appProcessContext :: !ProcessContext
  , appOptions        :: !Options
  , appConfig         :: !AppConfig
  , appTextures       :: !Textures
  , appRenderer       :: !Renderer
  }


data AppConfig = AppConfig
  { _appConfigStarLimit   :: !Int
  , _appConfigGalaxySize  :: !(Int, Int)  
  }
  deriving (Show, Read)


newtype Tick = MkTick { _unTick :: Word32 }
  deriving (Show, Read, Eq, Ord, Num, Real, Enum, Integral)


instance HasLogFunc App where
  logFuncL = lens appLogFunc (\x y -> x { appLogFunc = y })
instance HasProcessContext App where
  processContextL = lens appProcessContext (\x y -> x { appProcessContext = y })


makeLenses ''Tick
makeLenses ''AppConfig
makeLenses ''Options
