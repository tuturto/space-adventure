{-|
Module      : Run
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module Run
  ( run
  , runApplication
  , loadResources
  , freeResources
  ) where

import Import
import Control.Monad.Random.Strict
import qualified RIO.List as L
import RIO.Process
import SDL
import qualified SDL.Image as Image
import qualified Paths_space_adventure
import Components.Base
import System
import qualified UI.MainMenu.View
import qualified UI.SpaceStation.View
import Colours
import Transitions
import UI.Types
import Components.Types


-- |Start RIO application
runApplication :: Options -> (Textures, Renderer, Window) -> IO ()
runApplication options (textures, renderer, _) = do
  lo <- logOptionsHandle stderr (options ^. optionsVerbose)
  pc <- mkDefaultProcessContext
  withLogFunc lo $ \lf ->
    let app = App
          { appLogFunc = lf
          , appProcessContext = pc
          , appOptions = options
          , appRenderer = renderer
          , appTextures = textures
          , appConfig = AppConfig
              { _appConfigStarLimit = 2000
              , _appConfigGalaxySize = (1280, 960)
              }
          }
     in runRIO app run


loadTexture :: Renderer -> FilePath -> IO Texture
loadTexture r f = do
  fName <- Paths_space_adventure.getDataFileName ("assets/textures/" <> f)
  Image.loadTexture r fName


-- |Load surface from textures directory. Given FilePath should be name of
-- a file inside 'assets/textures/' directory.
loadSurface :: FilePath -> IO Surface
loadSurface fileName = do
  fName <- Paths_space_adventure.getDataFileName ("assets/textures/" <> fileName)
  Image.load fName


-- |Initialize window for the game and load various texures and fonts
loadResources :: Options -> IO (Textures, Renderer, Window)
loadResources o = do
  SDL.initializeAll
  Image.initialize [Image.InitPNG]

  window <- createWindow "Space Adventure" 
                          (if o ^. optionsDevelopment
                            then (defaultWindow { windowInitialSize = o ^. optionsResolution
                                                , windowBorder = True
                                                , windowMode = Windowed
                                                })
                            else (defaultWindow { windowInitialSize = o ^. optionsResolution
                                                , windowBorder = if o ^. optionsWindowed
                                                                  then True
                                                                  else False
                                                , windowMode = if o ^. optionsWindowed
                                                                  then Windowed
                                                                  else FullscreenDesktop
                                                }))
  renderer <- createRenderer window (-1) defaultRenderer
  rendererLogicalSize renderer $= Just (V2 640 480)

  if not $ o ^. optionsDevelopment
    then do
      rendererScale renderer $= o ^. optionsRenderScale
      rendererViewport renderer $= (Just $ o ^. optionsViewport)
    else do
      rendererScale renderer $= V2 1 1
      rendererViewport renderer $= Just (Rectangle (P $ V2 0 0) (V2 640 480))

  bevel <- loadSurface "bevel.png"
  ridge <- loadSurface "frame_ridge.png"
  inset <- loadSurface "inset.png"

  redTemplate <- loadButtonTemplate RedButton
  greenTemplate <- loadButtonTemplate GreenButton
  blueTemplate <- loadButtonTemplate BlueButton
  greyTemplate <- loadButtonTemplate GreyButton
  disabledTemplate <- loadDisabledTemplate

  redText <- loadSurface "red-font.png"
  greenText <- loadSurface "green-font.png"
  blueText <- loadSurface "blue-font.png"
  greyText <- loadSurface "grey-font.png"
  disabledText <- loadSurface "disabled-font.png"

  smallStar <- loadSurface "star_small.png"
  star1 <- loadTexture renderer "star_small.png"

  let 
    txt = Textures
      { _texturesBevel = bevel
      , _texturesRidge = ridge
      , _texturesInset = inset
      , _texturesRedButton = redTemplate
      , _texturesGreenButton = greenTemplate
      , _texturesBlueButton = blueTemplate
      , _texturesGreyButton = greyTemplate
      , _texturesDisabledButton = disabledTemplate
      , _texturesRedText = redText
      , _texturesGreenText = greenText
      , _texturesBlueText = blueText
      , _texturesGreyText = greyText
      , _texturesDisabledText = disabledText
      , _texturesSmallStar = smallStar
      , _texturesStar1 = star1
      }

  return (txt
         , renderer
         , window
         )


loadButtonTemplate :: ButtonColour -> IO ButtonTemplate
loadButtonTemplate c = do
  let ct = colourToText c

  activeTexture <- loadSurface (ct <> "-active-button.png")
  defaultTexture <- loadSurface (ct <> "-default-button.png")
  symbols <- loadSymbolsTemplate c

  return $ ButtonTemplate
    { _buttonTemplateActive  = activeTexture
    , _buttonTemplateDefault = defaultTexture
    , _buttonTemplateSymbols = symbols
    }


loadSymbolsTemplate :: ButtonColour -> IO ButtonSymbols
loadSymbolsTemplate c = do
  let ct = colourToText c

  plusSymbol <- loadSurface (ct <> "-plus.png")
  minusSymbol <- loadSurface (ct <> "-minus.png")
  leftArrowSymbol <- loadSurface (ct <> "-left-arrow.png")
  rightArrowSymbol <- loadSurface (ct <> "-right-arrow.png")
  upArrowSymbol <- loadSurface (ct <> "-up-arrow.png")
  downArrowSymbol <- loadSurface (ct <> "-down-arrow.png")
  cwArrowSymbol <- loadSurface (ct <> "-cw-arrow.png")
  ccwArrowSymbol <- loadSurface (ct <> "-ccw-arrow.png")
  xSymbol <- loadSurface (ct <> "-x.png")

  return $ ButtonSymbols
    { _buttonSymbolsPlus       = plusSymbol
    , _buttonSymbolsMinus      = minusSymbol
    , _buttonSymbolsLeftArrow  = leftArrowSymbol
    , _buttonSymbolsRightArrow = rightArrowSymbol
    , _buttonSymbolsUpArrow    = upArrowSymbol
    , _buttonSymbolsDownArrow  = downArrowSymbol
    , _buttonSymbolsCWArrow    = cwArrowSymbol
    , _buttonSymbolsCWWArrow   = ccwArrowSymbol
    , _buttonSymbolsX          = xSymbol
    }


colourToText :: ButtonColour -> String
colourToText c =
  case c of
    RedButton -> 
      "red"

    GreenButton ->
      "green"

    BlueButton ->
      "blue"

    GreyButton ->
      "grey"


loadDisabledTemplate :: IO DisabledButtonTemplate
loadDisabledTemplate = do
  button <- loadSurface "disabled-button.png"

  plusSymbol <- loadSurface "disabled-plus.png"
  minusSymbol <- loadSurface "disabled-minus.png"
  leftArrowSymbol <- loadSurface "disabled-left-arrow.png"
  rightArrowSymbol <- loadSurface "disabled-right-arrow.png"
  upArrowSymbol <- loadSurface "disabled-up-arrow.png"
  downArrowSymbol <- loadSurface "disabled-down-arrow.png"
  cwArrowSymbol <- loadSurface "disabled-cw-arrow.png"
  ccwArrowSymbol <- loadSurface "disabled-ccw-arrow.png"
  xSymbol <- loadSurface "disabled-x.png"

  return $ DisabledButtonTemplate
    { _disabledButtonTemplateButton  = button
    , _disabledButtonTemplateSymbols = ButtonSymbols
                                        { _buttonSymbolsPlus       = plusSymbol
                                        , _buttonSymbolsMinus      = minusSymbol
                                        , _buttonSymbolsLeftArrow  = leftArrowSymbol
                                        , _buttonSymbolsRightArrow = rightArrowSymbol
                                        , _buttonSymbolsUpArrow    = upArrowSymbol
                                        , _buttonSymbolsDownArrow  = downArrowSymbol
                                        , _buttonSymbolsCWArrow    = cwArrowSymbol
                                        , _buttonSymbolsCWWArrow   = ccwArrowSymbol
                                        , _buttonSymbolsX          = xSymbol
                                        }
  }

-- |Free textures and fonts used by program
freeResources :: (Textures, Renderer, Window) -> IO ()
freeResources (textures, _, window) = do
  free textures
  destroyWindow window
  Image.quit
  SDL.quit


run :: RIO App ()
run = do
  logInfo "Entering main loop..."
  t <- ticks
  mm <- liftIO $ evalRandIO $ mainMenu (MkTick t)
  _ <- appLoop NoUITextures mm
  logInfo "Main loop finished"


-- |Main loop of the program
-- recurses until the exit condition has been reached
appLoop :: UITextures -> Model -> RIO App (Model, UITextures)
appLoop txt state = do
  t <- ticks
  txt' <- renderUI txt state
  (newState, finalTextures) <- processEvents txt' (MkTick t) state
  finalState <- liftIO $ evalRandIO $ processTime (MkTick t) newState
  appLoopRecurse finalTextures finalState


-- |Render current state of the program on screen
renderUI :: UITextures -> Model -> RIO App UITextures
renderUI textures model = do
  (ui, txt') <- uiView textures model
  render ui model
  return txt'


-- |Collect and process events
processEvents :: UITextures -> Tick -> Model -> RIO App (Model, UITextures)
processEvents textures t model = do
  pumpEvents
  events <- pollEvents
  foldM (handleEvent t) (model, textures) $ L.sortBy (\a b -> compare (eventTimestamp a) (eventTimestamp b)) events


-- |Process time based things
-- these include both user interface things like scrolling when holding down a button
-- and also time based events in the game world
processTime :: RandomGen g => Tick -> Model -> Rand g Model
processTime newTick model = do
  model' <- case model of
              GameInMainMenu oldTick _ ->
                UI.MainMenu.View.stepTime model oldTick newTick

              DockedInSpaceStation _ _ _ _ ->
                return model

              GameExiting _ ->
                return model

  return model'


-- |Recurse to 'appLoop' depending on the state of the program
-- If 'System.Model' is 'GameExiting', the game cleans resources and
-- shuts down.
appLoopRecurse :: UITextures -> Model -> RIO App (Model, UITextures)
appLoopRecurse textures model = do
  -- recurse based on game state or exit the game
  case model of
    GameInMainMenu _ _ ->
      appLoop textures model

    DockedInSpaceStation _ _ _ _ ->
      appLoop textures model

    GameExiting _ -> do
      logDebug "Freeing resources..."
      free textures
      logDebug "Game exiting..."
      return (model, textures)


handleEvent :: Tick -> (Model, UITextures) -> Event -> RIO App (Model, UITextures)
handleEvent t (model, txt) event = do
  c <- asks appConfig
  (ui, txt') <- uiView txt model
  let (msgs, newModel) = toMessage model ui event
  final <- liftIO $ evalRandIO $ handleMessages c t msgs newModel
  return (final, txt')


handleMessages :: RandomGen g => AppConfig -> Tick -> [Message] -> Model -> Rand g Model
handleMessages _ _ [] model =
  return model

handleMessages c t (msg:msgs) model = do
  newModel <- update c t msg model
  handleMessages c t msgs newModel


-- |Transforms UI event into domain messages
toMessage :: Model -> Container -> Event -> ([Message], Model)
toMessage model ui event =
  case eventPayload event of
    MouseButtonEvent eventData ->
      let P (V2 x0 y0) = mouseButtonEventPos eventData
          dir = mouseButtonEventMotion eventData
          comp = componentAt (Coordinates (fromIntegral x0) (fromIntegral y0)) ui
      in case dir of
        Released ->
          fireEvents [ doMouseUp (Coordinates (fromIntegral x0) (fromIntegral y0))
                     , doClick (Coordinates (fromIntegral x0) (fromIntegral y0))
                     ]
                     comp
                     model

        Pressed ->
          fireEvents [ doMouseDown (Coordinates (fromIntegral x0) (fromIntegral y0)) ]
                     comp
                     model

    MouseMotionEvent _ ->
      --let heldButtons = mouseMotionEventState eventData
      --    P (V2 x0 y0) = mouseMotionEventPos eventData
      --    comp = componentAt (Coordinates (fromIntegral x0) (fromIntegral y0)) ui
      --in
        ([], model)

    WindowClosedEvent _ ->
      ([ QuitRequested ], model)

    QuitEvent  ->
      ([ QuitRequested ], model)

    _ ->
      ([], model)


-- | Trigger events for given UI Component and update model accordingly
-- Model updates here should deal with UI things, like changing button state and such
-- Actual domain logic is triggered later based on the list of messages produced here
fireEvents :: [Model -> UIComponent -> (Maybe Message, Model)] -> Maybe UIComponent -> Model -> ([Message], Model)
fireEvents _ Nothing model = 
  ([], model)

fireEvents funcs (Just c) model = 
  foldr (\f (accMsgs, accModel) ->
              case f accModel c of
                (Just m, newModel) ->
                  (accMsgs <> [m], newModel)
                      
                (Nothing, newModel) ->
                  (accMsgs, newModel))

        ([], model)
        funcs


-- |Updates model based on a domain message
update :: RandomGen g => AppConfig -> Tick -> Message -> Model -> Rand g Model
update c t msg model = 
  case msg of
    MainMenuMessage mm ->
      UI.MainMenu.View.update c t model mm

    SpaceStationMessage sm ->
      UI.SpaceStation.View.update model sm

    QuitRequested ->
      return $ GameExiting t

    -- TODO: ComponentMessages


-- |Create UI layout based on the current model
uiView :: UITextures -> Model -> RIO App (Container, UITextures)
uiView txt (GameInMainMenu _ mm) =
  UI.MainMenu.View.uiView txt mm

uiView txt (DockedInSpaceStation _ sm sId gd) = do
  UI.SpaceStation.View.uiView txt sm sId gd

uiView t (GameExiting _) = do
  free t
  return 
    ( Container
      { _containerTopLeft = Coordinates 0 0
      , _containerSize = Size 640 480
      , _containerName = "background"
      , _containerBackground = SolidColourBackground Colours.black
      , _containerOnClick = Nothing
      , _containerOnMouseDown = Nothing
      , _containerOnMouseUp = Nothing
      , _containerList = []
      }
    , NoUITextures
    )


-- |Render current UI on screen
render :: Container -> Model -> RIO App ()
render ui m = do
  renderer <- asks appRenderer
  rendererDrawColor renderer $= Colours.black
  clear renderer

  draw (Coordinates 0 0) m ui
  present renderer
