{-|
Module      : Model
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module Model 
  ( Atmosphere(..)
  , AtmosphereType(..)
  , AvailabilityRating(..)
  , Credits(..)
  , GoodsAmount(..)
  , GovermentType(..)
  , HabitationLevel(..)
  , HyperCoordinates(..)
  , Legality(..)
  , Lifeforms(..)
  , LuminosityClass(..)
  , Market(..)
  , MarketEntry(..)
  , MarketEvent(..)
  , Moon(..)
  , MoonId(..)
  , MoonName(..)
  , Planet(..)
  , PlanetId(..)
  , PlanetName(..)
  , PlanetType(..)  
  , PlanetZoneType(..)
  , SpaceStation(..)
  , SpaceStationId(..)
  , SpaceStationName(..)
  , SpaceStationType(..)
  , SpectralClass(..)
  , Star(..)
  , StarId(..)
  , StarName(..)
  , StarSystem(..)
  , StarSystemId(..)
  , StarSystemName(..)
  , TechLevel(..)
  , TradeGoods(..)
  , _GoodsInHighDemand
  , _GoodsInLowDemand  
  , _TraceAtmosphere
  , _ThinAtmosphere
  , _StandardAtmosphere
  , _HighAtmosphere
  , _VeryHighAtmosphere
  , unCredits
  , unGoodsAmount
  , unMoonId
  , unMoonName
  , unPlanetId
  , unPlanetName
  , unSpaceStationId
  , unSpaceStationName
  , unStarId
  , unStarName
  , unStarSystemId
  , unStarSystemName  
  , marketEntries
  , marketEvents 
  , marketEntryItem
  , marketEntrySellPrice
  , marketEntryPurchasePrice
  , marketEntryAmount
  , moonId
  , moonName
  , planetAtmostphere
  , planetGovermentType
  , planetHighestLifeForms
  , planetMoons
  , planetName
  , planetHabitation
  , planetId
  , planetSpaceStations
  , planetTechLevel
  , planetType
  , spaceStationId
  , spaceStationName
  , spaceStationMarket
  , spaceStationTechLevel
  , spaceStationType
  , starId
  , starLuminosityClass
  , starName
  , starSpectralClass
  , starSystemId
  , starSystemLocation
  , starSystemName
  , starSystemPlanets
  , starSystemSpaceStations
  , starSystemStars
  , tradeGoodName

  , spaceStation'sSystem
  , systemContainsStation
  , systemStations
  ) where

import RIO
import Control.Lens 
  ( Fold
  , elemOf
  , filtered
  , firstOf
  , folded
  , folding
  , makeLenses
  , makePrisms
  )
import Data.Aeson.TH ( deriveJSON, defaultOptions )


newtype SpaceStationId = MkSpaceStationId { _unSpaceStationId :: Int }
  deriving (Show, Read, Eq)


data TradeGoods =
  Food
  | Textiles
  | Radioactives
  | Liquor
  | Luxuries
  | Narcotics
  | Computers
  | Machinery
  | Alloys
  | Firearms
  | Furs
  | Minerals
  | Gold
  | Platinum
  | GemStones
  | AlienItems
  | GenderFluid
  deriving (Show, Read, Eq, Enum, Bounded, Ord)


data GovermentType =
  DirectDemocracy
  | RepublicGovernment
  | ParliamentaryGovernment
  | Authoritarian
  | Totalitarian
  | Monarchy
  | Oligarchy
  | Technocracy
  | Theocracy
  | Dictatorship
  | Communist
  | Aristocratic
  deriving (Show, Read, Eq, Enum, Bounded, Ord)


data TechLevel =
  Primitive
  | LowTech
  | CommonTech
  | HighTech
  | AdvancedTech
  | HyperAdvancedTech
  deriving (Show, Read, Eq, Enum, Bounded, Ord)


data AvailabilityRating =
  VeryCommonAvailability
  | CommonAvailability
  | UncommonAvailability
  | RareAvailability
  | VeryRareAvailability
  | UniqueAvailability
  deriving (Show, Read, Eq, Enum, Bounded, Ord)


tradeGoodName :: TradeGoods -> Text
tradeGoodName Food = "food"
tradeGoodName Textiles = "textiles"
tradeGoodName Radioactives = "radioactives"
tradeGoodName Liquor = "liquor"
tradeGoodName Luxuries = "luxuries"
tradeGoodName Narcotics = "narcotics"
tradeGoodName Computers = "computers"
tradeGoodName Machinery = "machinery"
tradeGoodName Alloys = "alloys"
tradeGoodName Firearms = "firearms"
tradeGoodName Furs = "furs"
tradeGoodName Minerals = "minerals"
tradeGoodName Gold = "gold"
tradeGoodName Platinum = "platinum"
tradeGoodName GemStones = "gemstones"
tradeGoodName AlienItems = "alien items"
tradeGoodName GenderFluid = "gender fluid"


data Legality =
  Legal
  | Illegal
  deriving (Show, Read, Eq, Enum, Bounded, Ord)


newtype Credits = MkCredits { _unCredits :: Int }
  deriving (Show, Read, Eq, Num, Enum, Ord, Real, Integral)


data MarketEntry = MarketEntry
  { _marketEntryItem          :: !TradeGoods
  , _marketEntrySellPrice     :: !Credits
  , _marketEntryPurchasePrice :: !Credits
  , _marketEntryAmount        :: !GoodsAmount
  }
  deriving (Show, Read, Eq)


data Market = Market
  { _marketEntries :: ![MarketEntry]
  , _marketEvents  :: ![MarketEvent]
  }
  deriving (Show, Read, Eq)


data MarketEvent =
  GoodsInHighDemand TradeGoods
  | GoodsInLowDemand TradeGoods
  deriving (Show, Read, Eq)


newtype GoodsAmount = MkGoodsAmount { _unGoodsAmount :: Natural }
  deriving (Show, Read, Eq, Num, Enum, Integral, Ord, Real)


data StarSystem = StarSystem
  { _starSystemId            :: !StarSystemId
  , _starSystemName          :: !StarSystemName
  , _starSystemLocation      :: !HyperCoordinates
  , _starSystemStars         :: ![Star]
  , _starSystemPlanets       :: ![Planet]
  , _starSystemSpaceStations :: ![SpaceStation]
  }
  deriving (Show, Read, Eq)


newtype StarSystemId = MkStarSystemId { _unStarSystemId :: Int }
  deriving (Show, Read, Eq)


newtype StarSystemName = MkStarSystemName { _unStarSystemName :: Text }
  deriving (Show, Read, Eq, IsString)


data HyperCoordinates = 
  HyperCoordinates Int Int
  deriving (Show, Read, Eq)


data SpaceStation = SpaceStation
  { _spaceStationId        :: !SpaceStationId
  , _spaceStationName      :: !SpaceStationName
  , _spaceStationType      :: !SpaceStationType
  , _spaceStationTechLevel :: !TechLevel
  , _spaceStationMarket    :: !Market
  }
  deriving (Show, Read, Eq)


newtype SpaceStationName = MkSpaceStationName { _unSpaceStationName :: Text }
  deriving (Show, Read, Eq, IsString)


data SpaceStationType =
  MiningStation
  | ResearchStation
  | TradingPost
  | MilitaryBase
  deriving (Show, Read, Eq, Ord, Bounded, Enum)


data Planet = Planet
  { _planetId                  :: !PlanetId
  , _planetName                :: !PlanetName
  , _planetType                :: !PlanetType
  , _planetAtmostphere         :: !Atmosphere
  , _planetHighestLifeForms    :: !(Maybe Lifeforms)
  , _planetHabitation          :: !HabitationLevel
  , _planetGovermentType       :: !GovermentType
  , _planetTechLevel           :: !TechLevel
  , _planetSpaceStations       :: ![SpaceStation]
  , _planetMoons               :: ![Moon]
  }
  deriving (Show, Read, Eq)


newtype PlanetId = MkPlanetId { _unPlanetId :: Int }
  deriving (Show, Read, Eq)


newtype PlanetName = MkPlanetName { _unPlanetName :: Text }
  deriving (Show, Read, Eq, IsString)


data PlanetType =
  Geothermal
  | Geomorteus
  | Geoinactive
  | Asteroid
  | Geoplastic
  | Geometallic
  | Geocrystalline
  | Desert
  | GasSuperGiant
  | GasGiant
  | Adaptable
  | Marginal
  | Terrestrial
  | Reducing
  | Pelagic
  | Glaciated
  | Variable
  | Rogue
  | Ultragiant
  | Demon
  deriving (Show, Read, Eq, Ord, Bounded, Enum)


data Atmosphere =
  VacuumAtmosphere
  | TraceAtmosphere AtmosphereType
  | ThinAtmosphere AtmosphereType
  | StandardAtmosphere AtmosphereType
  | HighAtmosphere AtmosphereType
  | VeryHighAtmosphere AtmosphereType
  deriving (Show, Read, Eq, Ord)


data AtmosphereType =
  BreathableAtmosphere
  | CausticAtmosphere
  | RadiologicalAtmosphere
  | PoisonousAtmosphere
  deriving (Show, Read, Eq, Ord, Bounded, Enum)


data Lifeforms =
  Bacteria
  | Protozoa
  | Fungi
  | Fish
  | Amphibians
  | Reptiles
  | Birds
  | Mammals
  deriving (Show, Read, Eq, Ord, Bounded, Enum)


data HabitationLevel =
  NoHabitation
  | SmallColonies
  | BigColonies
  | SparseHabitation
  | MediumHabitation
  | DenseHabitation
  deriving (Show, Read, Eq, Ord, Bounded, Enum)


newtype MoonId = MkMoonId { _unMoonId :: Int }
  deriving (Show, Read, Eq)


newtype MoonName = MkMoonName { _unMoonName :: Text }
  deriving (Show, Read, Eq, IsString)


data Moon = Moon
  { _moonId   :: !MoonId
  , _moonName :: !MoonName
  }
  deriving (Show, Read, Eq)


data Star = Star
  { _starId              :: !StarId
  , _starName            :: !StarName
  , _starSpectralClass   :: !SpectralClass
  , _starLuminosityClass :: !LuminosityClass
  }
  deriving (Show, Read, Eq)


newtype StarId = MkStarId { _unStarId :: Int }
  deriving (Show, Read, Eq)


newtype StarName = MkStarName { _unStarName :: Text }
  deriving (Show, Read, Eq, IsString)


data SpectralClass =
  SpectralO
  | SpectralB
  | SpectralA
  | SpectralF
  | SpectralG
  | SpectralK
  | SpectralM
  | SpectralL
  | SpectralT
  deriving (Show, Read, Eq, Enum, Bounded, Ord)


data LuminosityClass =
  Luminosity0       -- hyper giant
  | LuminosityI     -- super giant
  | LuminosityII    -- bright giant
  | LuminosityIII   -- giant
  | LuminosityIV    -- subgiant
  | LuminosityV     -- main-sequence
  | LuminosityVI    -- subdwarf
  | LuminosityVII   -- white dwarf
  deriving (Show, Read, Eq, Enum, Bounded, Ord)


data PlanetZoneType =
  HotZone
  | EcoZone
  | ColdZone
  | CometaryHalo
  deriving (Show, Read, Eq, Enum, Bounded, Ord)


makePrisms ''Atmosphere
makeLenses ''Credits
makePrisms ''MarketEvent


makeLenses ''GoodsAmount
makeLenses ''HyperCoordinates
makeLenses ''Market
makeLenses ''MarketEntry
makeLenses ''Moon
makeLenses ''MoonId
makeLenses ''MoonName
makeLenses ''Planet
makeLenses ''PlanetId
makeLenses ''PlanetName
makeLenses ''SpaceStation
makeLenses ''SpaceStationId
makeLenses ''SpaceStationName
makeLenses ''Star
makeLenses ''StarId
makeLenses ''StarName
makeLenses ''StarSystem
makeLenses ''StarSystemId
makeLenses ''StarSystemName

deriveJSON defaultOptions ''Atmosphere
deriveJSON defaultOptions ''AtmosphereType
deriveJSON defaultOptions ''AvailabilityRating
deriveJSON defaultOptions ''Credits

deriveJSON defaultOptions ''GoodsAmount
deriveJSON defaultOptions ''GovermentType
deriveJSON defaultOptions ''HabitationLevel
deriveJSON defaultOptions ''HyperCoordinates
deriveJSON defaultOptions ''Legality
deriveJSON defaultOptions ''Lifeforms
deriveJSON defaultOptions ''LuminosityClass
deriveJSON defaultOptions ''Market
deriveJSON defaultOptions ''MarketEntry
deriveJSON defaultOptions ''MarketEvent
deriveJSON defaultOptions ''Moon
deriveJSON defaultOptions ''MoonId
deriveJSON defaultOptions ''MoonName
deriveJSON defaultOptions ''Planet
deriveJSON defaultOptions ''PlanetId
deriveJSON defaultOptions ''PlanetName
deriveJSON defaultOptions ''PlanetType
deriveJSON defaultOptions ''PlanetZoneType
deriveJSON defaultOptions ''SpaceStation
deriveJSON defaultOptions ''SpaceStationId
deriveJSON defaultOptions ''SpaceStationName
deriveJSON defaultOptions ''SpaceStationType
deriveJSON defaultOptions ''SpectralClass
deriveJSON defaultOptions ''Star
deriveJSON defaultOptions ''StarId
deriveJSON defaultOptions ''StarName
deriveJSON defaultOptions ''StarSystem
deriveJSON defaultOptions ''StarSystemId
deriveJSON defaultOptions ''StarSystemName
deriveJSON defaultOptions ''TechLevel
deriveJSON defaultOptions ''TradeGoods


-- |System which has the given space station. If space station is present in
-- multiple systems, only the first one will be returned
spaceStation'sSystem :: [ StarSystem ] -> SpaceStation -> Maybe StarSystem
spaceStation'sSystem s st =
  firstOf (folded . filtered (systemContainsStation st)) s


-- |Is a given space station located inside a given star system?
systemContainsStation :: SpaceStation -> StarSystem -> Bool
systemContainsStation station system =
    elemOf (systemStations . spaceStationId) sId system
  where
    sId = station ^. spaceStationId


-- |All space stations of a star system
systemStations :: Fold StarSystem SpaceStation
systemStations =
  folding (\s -> s ^.. starSystemSpaceStations . folded
              <> s ^.. starSystemPlanets . folded . planetSpaceStations . folded)
