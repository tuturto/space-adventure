{-|
Module      : Components.StarfieldViewModels
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module Components.StarfieldViewModels
  ( StarfieldVM(..)
  , Star(..)
  , initializeStarField
  , starfieldBorderX
  , starfieldBorderY
  , starfieldCenter
  , starfieldMaxSpeed
  , starfieldVMStars
  , starfieldVMStarCount
  , starCoordinates
  , starVelocity
  , starSpeed
  , stepTime
  , unBorders
  , unStarCoordinates
  , unStarCount
  , unStarVelocity
  )

where

import Import
import Control.Lens ( makeLenses )
import Control.Monad.Random.Strict


data StarfieldVM = StarfieldVM
  { _starfieldVMStarCount :: !StarCount
  , _starfieldMaxSpeed    :: !(Double, Double)
  , _starfieldVMStars     :: ![Star]
  , _starfieldBorderX     :: !Borders
  , _starfieldBorderY     :: !Borders
  , _starfieldCenter      :: !StarCoordinates
  }


newtype StarCount = MkStarCount { _unStarCount :: Int }
  deriving (Show, Read, Eq, Ord, Num, Enum, Real, Integral)


data Star = Star
  { _starCoordinates :: StarCoordinates
  , _starVelocity    :: StarVelocity
  , _starSpeed       :: Double
  }
  deriving (Show, Read, Eq)


newtype StarCoordinates = MkStarCoordinates { _unStarCoordinates :: (Double, Double)}
  deriving (Show, Read, Eq)


newtype StarVelocity = MkStarVelocity { _unStarVelocity :: (Double, Double)}
  deriving (Show, Read, Eq)


newtype Borders = MkBorders { _unBorders :: (Double, Double)}
  deriving (Show, Read, Eq)


stepTime :: RandomGen g => StarfieldVM -> Tick -> Tick -> Rand g StarfieldVM
stepTime vm ot nt = do 
  stars <- mapM (updateStar vm $ nt - ot) (_starfieldVMStars vm)
  return $ vm { _starfieldVMStars = stars }


initializeStarField :: RandomGen g => StarCount -> (Double, Double) -> (Double, Double) -> Rand g StarfieldVM
initializeStarField sc (w, h) speed = do
  let
    cx = w / 2
    cy = h / 2
    center = MkStarCoordinates (cx,cy)

  stars <- replicateM (fromIntegral sc) (generateStar center speed)

  let
    vm = StarfieldVM
      { _starfieldVMStarCount = sc
      , _starfieldMaxSpeed    = speed
      , _starfieldVMStars     = stars
      , _starfieldBorderX     = MkBorders (0, w)
      , _starfieldBorderY     = MkBorders (0, h)
      , _starfieldCenter      = center
      }

  stepTime vm 0 5000


updateStar :: RandomGen g => StarfieldVM -> Tick -> Star -> Rand g Star
updateStar vm dt star = do
  let
    MkStarCoordinates (x, y) = updLocation dt star
    (lx, ux) = _unBorders $ _starfieldBorderX vm
    (ly, uy) = _unBorders $ _starfieldBorderY vm

  if x < lx || x > ux || y < ly || y > uy
    then
      resetStar (_starfieldCenter vm) star
    else
      return $ star { _starCoordinates = MkStarCoordinates (x, y) }


updLocation :: Tick -> Star -> StarCoordinates
updLocation dt star =
  MkStarCoordinates (ox + (vx * (fromIntegral dt)/1000), oy + (vy * (fromIntegral dt)/1000))
  where
    MkStarCoordinates (ox, oy) = _starCoordinates star
    MkStarVelocity (vx, vy) = _starVelocity star


resetStar :: RandomGen g => StarCoordinates -> Star -> Rand g Star
resetStar center star = do
  angle <- getRandomR (0, 2 * pi)
  let 
    velocity = MkStarVelocity $
                  ( _starSpeed star * sin angle
                  , _starSpeed star * cos angle
                  )
  return $ star
    { _starCoordinates = center
    , _starVelocity    = velocity
    }


generateStar :: RandomGen g => StarCoordinates -> (Double, Double) -> Rand g Star
generateStar center (ls, ms) = do
  angle <- getRandomR (0, 2 * pi)
  speed <- getRandomR (ls, ms)
  let 
    velocity = MkStarVelocity $
                  ( speed * sin angle
                  , speed * cos angle
                  )
  return $ Star
    { _starCoordinates = center
    , _starVelocity    = velocity
    , _starSpeed       = speed
    }


makeLenses ''Borders
makeLenses ''StarfieldVM
makeLenses ''StarCount
makeLenses ''Star
makeLenses ''StarCoordinates
makeLenses ''StarVelocity
