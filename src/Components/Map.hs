{-|
Module      : Components.Map
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module Components.Map
  ( StarMap(..)
  , StarMapConfig(..)
  , SystemMap(..)
  , MapControlsConfig(..)
  , MapInfoDisplayConfig(..)
  , createStarMap
  , mapControls  
  , mapControlsConfigTopLeft
  , mapControlsConfigSize
  , mapControlsConfigName
  , mapControlsConfigZoomChanged
  , mapControlsConfigUpButton
  , mapControlsConfigRightButton
  , mapControlsConfigLeftButton
  , mapControlsConfigDownButton
  , mapControlsConfigTextures
  , mapControlsConfigUpButtonVM
  , mapControlsConfigDownButtonVM
  , mapControlsConfigLeftButtonVM
  , mapControlsConfigRightButtonVM
  , mapControlsConfigZoomButtonVM
  , mapInfoDisplay
  , mapInfoDisplayConfigTopLeft
  , mapInfoDisplayConfigSize
  , mapInfoDisplayConfigName
  , mapInfoDisplayConfigTextures
  , starMap
  , starMapConfigTopLeft
  , starMapConfigSize
  , starMapConfigBackground
  , starMapConfigName
  , starMapConfigVM
  , starMapConfigZoomChanged
  , starMapConfigUpButton
  , starMapConfigStarSelected
  , starMapConfigRightButton
  , starMapConfigLeftButton
  , starMapConfigDownButton
  , starMapConfigTextures
  , starMapConfigUpButtonVM
  , starMapConfigDownButtonVM
  , starMapConfigLeftButtonVM
  , starMapConfigRightButtonVM
  , starMapConfigZoomButtonVM  
  , starMapTopLeft
  , starMapSize
  , starMapBackground
  , starMapName
  , starMapVM
  , starMapStarSelected
  , starMapStars
  , systemMap
  , systemMapTopLeft
  , systemMapSize
  , systemMapBackground
  , systemMapName
  , systemMapList
  , systemMapOnClick
  , systemMapOnMouseDown
  , systemMapOnMouseUp
  )
  where

import Control.Lens ( makeLenses )
import SDL
import Import
import Graphics
import UI.Types
import Components.Base
import Components.BaseViewModels
import Components.MapViewModels
import Components.Types
import Model


-- |Star map with accompanying map controls and an info box
starMap :: StarMapConfig -> UIComponent
starMap c = 
  pack $
    Container
      { _containerTopLeft     = _starMapConfigTopLeft c
      , _containerSize        = _starMapConfigSize c
      , _containerBackground  = _starMapConfigBackground c
      , _containerName        = _starMapConfigName c
      , _containerList        = [ 
                                -- TODO: add map info display when user has selected a star?
                                mapInfoDisplay $ 
                                    MapInfoDisplayConfig
                                      { _mapInfoDisplayConfigTopLeft  = Coordinates (sizeX - 60) 0
                                      , _mapInfoDisplayConfigSize     = Size 60 60
                                      , _mapInfoDisplayConfigName     = _starMapConfigName c <> " - map info"
                                      , _mapInfoDisplayConfigTextures = t ^. starmapTexturesInfoPanel
                                      }
                                , mapControls $ 
                                    MapControlsConfig
                                      { _mapControlsConfigTopLeft       = Coordinates (sizeX - 65) (sizeY - 65)
                                      , _mapControlsConfigSize          = Size 60 60
                                      , _mapControlsConfigName          = _starMapConfigName c <> " - map controls"
                                      , _mapControlsConfigUpButton      = _starMapConfigUpButton c
                                      , _mapControlsConfigDownButton    = _starMapConfigDownButton c
                                      , _mapControlsConfigLeftButton    = _starMapConfigLeftButton c
                                      , _mapControlsConfigRightButton   = _starMapConfigRightButton c
                                      , _mapControlsConfigZoomChanged   = _starMapConfigZoomChanged c
                                      , _mapControlsConfigTextures      = t ^. starmapTexturesControls     
                                      , _mapControlsConfigUpButtonVM    = _starMapConfigUpButtonVM c
                                      , _mapControlsConfigDownButtonVM  = _starMapConfigDownButtonVM c
                                      , _mapControlsConfigLeftButtonVM  = _starMapConfigLeftButtonVM c
                                      , _mapControlsConfigRightButtonVM = _starMapConfigRightButtonVM c
                                      , _mapControlsConfigZoomButtonVM  = _starMapConfigZoomButtonVM c
                                      }
                                , pack $ StarMap
                                  { _starMapTopLeft      = Coordinates 0 0
                                  , _starMapSize         = Size sizeX sizeY
                                  , _starMapBackground   = BackgroundTexture $ t ^. starMapTexturesBackground
                                  , _starMapName         = _starMapConfigName c <> " - map display"
                                  , _starMapVM           = _starMapConfigVM c
                                  , _starMapStarSelected = _starMapConfigStarSelected c
                                  , _starMapStars        = t ^. starMapTexturesStars
                                  }
                                ]
      , _containerOnClick     = Nothing
      , _containerOnMouseDown = Nothing
      , _containerOnMouseUp   = Nothing
      }
  where
    t = _starMapConfigTextures c
    Size sizeX sizeY = _starMapConfigSize c


-- |System map with accompanying map controls and an info box
systemMap :: UIComponent
systemMap = undefined


data StarMapConfig = StarMapConfig
  { _starMapConfigTopLeft       :: !Coordinates
  , _starMapConfigSize          :: !Size
  , _starMapConfigBackground    :: !Background
  , _starMapConfigName          :: !Text
  , _starMapConfigVM            :: !(VMSelector StarMapVM)
  , _starMapConfigUpButton      :: !(Maybe Message)
  , _starMapConfigDownButton    :: !(Maybe Message)
  , _starMapConfigLeftButton    :: !(Maybe Message)
  , _starMapConfigRightButton   :: !(Maybe Message)
  , _starMapConfigStarSelected  :: !(Maybe Message)
  , _starMapConfigZoomChanged   :: !(Maybe Message)
  , _starMapConfigTextures      :: !StarmapTextures
  , _starMapConfigUpButtonVM    :: !(VMSelector ButtonVM)
  , _starMapConfigDownButtonVM  :: !(VMSelector ButtonVM)
  , _starMapConfigLeftButtonVM  :: !(VMSelector ButtonVM)
  , _starMapConfigRightButtonVM :: !(VMSelector ButtonVM)
  , _starMapConfigZoomButtonVM  :: !(VMSelector ButtonVM)
  }


data StarMap = StarMap
  { _starMapTopLeft      :: !Coordinates
  , _starMapSize         :: !Size
  , _starMapBackground   :: !Background
  , _starMapName         :: !Text
  , _starMapVM           :: !(VMSelector StarMapVM)
  , _starMapStarSelected :: !(Maybe Message)
  , _starMapStars        :: !Texture
  }


instance Component StarMap where
  -- |draw renders component with given coordinates being the top left corner
  draw globalCoords@(Coordinates x y) m c = do
    renderer <- asks appRenderer
    renderBackground renderer globalCoords (size c) (background c)
    let
      (Size w h) = size c
      (vmGet, _) = _starMapVM c
      vm_ = vmGet m
      (HyperCoordinates cx cy) = case vm_ of
                                  Nothing ->
                                    HyperCoordinates 0 0

                                  Just vm ->
                                    vm ^. starMapCenter
      (lx, ly) = (cx - w `divInt` 2, cy - h `divInt` 2)

    copy renderer (_starMapStars c) 
           (Just (Rectangle (P $ V2 (fromIntegral $ lx) (fromIntegral $ ly))
             (V2 (fromIntegral (w - 4)) (fromIntegral (h - 4)))))
           (Just (Rectangle (P $ V2 (fromIntegral $ x + 2) (fromIntegral $ y + 2)) 
             (V2 (fromIntegral (w - 4)) (fromIntegral (h - 4)))))


  -- |mouseDown fires when mouse button is pressed while cursor is over the component.
  -- mouseDown also fires when mouse button is pressed while cursor is somewhere else
  -- and then cursor is moved over the component before releasing the button.
  -- doMouseDown   :: Coordinates -> Model -> a -> (Maybe Message, Model)
  doMouseDown _ model _ = (Nothing, model)
--    fireIfEnabled model coords c onMouseDown

  -- |mouseUp fires when mouse button is released while cursor is over the component.
  -- doMouseUp     :: Coordinates -> Model -> a -> (Maybe Message, Model)
  doMouseUp _ model _ = (Nothing, model)
    -- fireIfEnabled model coords c onMouseUp

  -- |onClick fires when mouse button is pressed and then released over the Component.
  -- Before onClick fires, mouseUp is triggered.
  -- doClick     :: Coordinates -> Model -> a -> (Maybe Message, Model)
  doClick _ model _ = (Nothing, model)
    -- fireIfEnabled model coords c onClick

  -- onMouseDown :: a -> Maybe Message
  onMouseDown _ = Nothing

  -- onMouseUp :: a -> Maybe Message
  onMouseUp _ = Nothing

  -- onClick :: a -> Maybe Message
  onClick _ = Nothing

  topLeft = _starMapTopLeft

  size = _starMapSize

  -- background :: a -> Background
  background = _starMapBackground

  name = _starMapName

  -- active :: Model -> a -> ActiveState
  active _ _ = Enabled


-- |Create texture that shows a high level star map centered at given 'Model.HyperCoordinates'.
createStarMap :: Renderer -> Surface -> Size -> [StarSystem] -> RIO App Texture
createStarMap renderer star (Size w h) systems = do
  surface <- createRGBSurface (V2 (fromIntegral w) (fromIntegral h)) RGBA8888
  (V2 tw th) <- surfaceDimensions star
  let
    ox = (fromIntegral tw) `div` 2
    oy = (fromIntegral th) `div` 2

  mapM_ (\system -> do
                let
                  (HyperCoordinates lx ly) = system ^. starSystemLocation
                  sx = fromIntegral (lx - ox)
                  sy = fromIntegral (ly - oy)
                surfaceBlit star Nothing surface (Just $ P $ V2 sx sy)
                ) 
        systems

  texture <- createTextureFromSurface renderer surface
  free surface
  return texture


data SystemMap = SystemMap
  { _systemMapTopLeft     :: !Coordinates
  , _systemMapSize        :: !Size
  , _systemMapBackground  :: !Background
  , _systemMapName        :: !Text
  , _systemMapList        :: ![UIComponent]
  , _systemMapOnClick     :: !(Maybe Message)
  , _systemMapOnMouseDown :: !(Maybe Message)
  , _systemMapOnMouseUp   :: !(Maybe Message)
  }


instance Component SystemMap where
  -- |draw renders component with given coordinates being the top left corner
  draw globalCoords _ c = do
    -- TODO: implement rest
    renderer <- asks appRenderer
    renderBackground renderer globalCoords (size c) (background c)

  -- |mouseDown fires when mouse button is pressed while cursor is over the component.
  -- mouseDown also fires when mouse button is pressed while cursor is somewhere else
  -- and then cursor is moved over the component before releasing the button.
  -- doMouseDown   :: Coordinates -> Model -> a -> (Maybe Message, Model)
  doMouseDown _ _ _ = undefined
--    fireIfEnabled model coords c onMouseDown

  -- |mouseUp fires when mouse button is released while cursor is over the component.
  -- doMouseUp     :: Coordinates -> Model -> a -> (Maybe Message, Model)
  doMouseUp _ _ _ = undefined
    -- fireIfEnabled model coords c onMouseUp

  -- |onClick fires when mouse button is pressed and then released over the Component.
  -- Before onClick fires, mouseUp is triggered.
  -- doClick     :: Coordinates -> Model -> a -> (Maybe Message, Model)
  doClick _ _ _ = undefined
    -- fireIfEnabled model coords c onClick

  -- onMouseDown :: a -> Maybe Message
  onMouseDown _ = Nothing

  -- onMouseUp :: a -> Maybe Message
  onMouseUp _ = Nothing

  -- onClick :: a -> Maybe Message
  onClick _ = Nothing

  topLeft = _systemMapTopLeft

  size = _systemMapSize

  -- background :: a -> Background
  background = undefined

  name = _systemMapName

  -- active :: Model -> a -> ActiveState
  active _ _ = Enabled


mapInfoDisplay :: MapInfoDisplayConfig -> UIComponent
mapInfoDisplay _ = pack $ NoComponent


data MapInfoDisplayConfig = MapInfoDisplayConfig
  { _mapInfoDisplayConfigTopLeft     :: !Coordinates
  , _mapInfoDisplayConfigSize        :: !Size
  , _mapInfoDisplayConfigName        :: !Text
  , _mapInfoDisplayConfigTextures    :: !MapInfoPanelTextures
  }


mapControls :: MapControlsConfig -> UIComponent
mapControls c =
  pack $
    Container
      { _containerTopLeft     = _mapControlsConfigTopLeft c
      , _containerSize        = _mapControlsConfigSize c
      , _containerBackground  = BackgroundTexture $ _mapControlsTexturesBackground t
      , _containerName        = _mapControlsConfigName c
      , _containerList        = [ pack $ Button
                                    { _buttonTopLeft     = Coordinates 22 5
                                    , _buttonSize        = Size 16 16
                                    , _buttonName        = "Up button"
                                    , _buttonTexture     = t ^. mapControlsTexturesUp
                                    , _buttonOnMouseDown = Nothing
                                    , _buttonOnMouseUp   = Nothing
                                    , _buttonOnClick     = Nothing
                                    , _buttonVM          = _mapControlsConfigUpButtonVM c
                                    }
                                , pack $ Button
                                    { _buttonTopLeft     = Coordinates 39 22
                                    , _buttonSize        = Size 16 16
                                    , _buttonName        = "Right button"
                                    , _buttonTexture     = t ^. mapControlsTexturesRight
                                    , _buttonOnMouseDown = Nothing
                                    , _buttonOnMouseUp   = Nothing
                                    , _buttonOnClick     = Nothing
                                    , _buttonVM          = _mapControlsConfigRightButtonVM c
                                    }
                                , pack $ Button
                                    { _buttonTopLeft     = Coordinates 22 39
                                    , _buttonSize        = Size 16 16
                                    , _buttonName        = "Down button"
                                    , _buttonTexture     = t ^. mapControlsTexturesDown
                                    , _buttonOnMouseDown = Nothing
                                    , _buttonOnMouseUp   = Nothing
                                    , _buttonOnClick     = Nothing
                                    , _buttonVM          = _mapControlsConfigDownButtonVM c
                                    }
                                , pack $ Button
                                    { _buttonTopLeft     = Coordinates 5 22
                                    , _buttonSize        = Size 16 16
                                    , _buttonName        = "Left button"
                                    , _buttonTexture     = t ^. mapControlsTexturesLeft
                                    , _buttonOnMouseDown = Nothing
                                    , _buttonOnMouseUp   = Nothing
                                    , _buttonOnClick     = Nothing
                                    , _buttonVM          = _mapControlsConfigLeftButtonVM c
                                    }
                                , pack $ Button
                                    { _buttonTopLeft     = Coordinates 22 22
                                    , _buttonSize        = Size 16 16
                                    , _buttonName        = "Zoom button"
                                    , _buttonTexture     = t ^. mapControlsTexturesZoom
                                    , _buttonOnMouseDown = Nothing
                                    , _buttonOnMouseUp   = Nothing
                                    , _buttonOnClick     = Nothing
                                    , _buttonVM          = _mapControlsConfigZoomButtonVM c
                                    }
                                ]
      , _containerOnClick     = Nothing
      , _containerOnMouseDown = Nothing
      , _containerOnMouseUp   = Nothing
      }
  where
    t = _mapControlsConfigTextures c


data MapControlsConfig = MapControlsConfig
  { _mapControlsConfigTopLeft       :: !Coordinates
  , _mapControlsConfigSize          :: !Size
  , _mapControlsConfigName          :: !Text
  , _mapControlsConfigUpButton      :: !(Maybe Message)
  , _mapControlsConfigDownButton    :: !(Maybe Message)
  , _mapControlsConfigLeftButton    :: !(Maybe Message)
  , _mapControlsConfigRightButton   :: !(Maybe Message)
  , _mapControlsConfigZoomChanged   :: !(Maybe Message)
  , _mapControlsConfigTextures      :: !MapControlsTextures
  , _mapControlsConfigUpButtonVM    :: !(VMSelector ButtonVM)
  , _mapControlsConfigDownButtonVM  :: !(VMSelector ButtonVM)
  , _mapControlsConfigLeftButtonVM  :: !(VMSelector ButtonVM)
  , _mapControlsConfigRightButtonVM :: !(VMSelector ButtonVM)
  , _mapControlsConfigZoomButtonVM  :: !(VMSelector ButtonVM)
  }


makeLenses ''StarMap
makeLenses ''StarMapConfig
makeLenses ''SystemMap
makeLenses ''MapInfoDisplayConfig
makeLenses ''MapControlsConfig
