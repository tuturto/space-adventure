{-|
Module      : Components.Starfield
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module Components.Starfield
  ( Starfield(..)
  , starfieldTopLeft
  , starfieldSize
  , starfieldName
  , starfieldBackground
  , starfieldVM
  )

where

import Import
import SDL
import Control.Lens ( makeLenses )
import Graphics
import UI.Types
import Components.Base
import Components.StarfieldViewModels


data Starfield = Starfield 
  { _starfieldTopLeft    :: !Coordinates
  , _starfieldSize       :: !Size
  , _starfieldName       :: !Text
  , _starfieldBackground :: !Background
  , _starfieldVM         :: !(VMSelector StarfieldVM)
  }


instance Component Starfield where
  draw globalCoords@(Coordinates gx gy) m c = do
    options <- asks appOptions
    renderer <- asks appRenderer
    textures <- asks appTextures

    let
      (Coordinates lx ly) = topLeft c
      (getter, _) = _starfieldVM c
      stars = _starfieldVMStars <$> (getter m) :: Maybe [Star]
    
    setViewport (Coordinates (gx + lx) (gy + ly)) (size c)

    renderBackground renderer globalCoords (size c) (background c)

    (mapM_ . mapM_) (drawStar renderer textures) stars

    resetViewport options renderer
  
  onMouseDown _ = Nothing

  onMouseUp _ = Nothing

  onClick _ = Nothing

  topLeft = _starfieldTopLeft

  size = _starfieldSize

  background = _starfieldBackground

  name = _starfieldName

  active _ _ = Enabled


drawStar :: Renderer -> Textures -> Star -> RIO App ()
drawStar renderer textures star = do
  let
    (x, y) = star ^. starCoordinates . unStarCoordinates

  copy renderer
       (textures ^. texturesStar1)
       Nothing
       (Just (Rectangle (P (V2 (round x) (round y))) 
             (V2 3 3)))

       


makeLenses ''Starfield
