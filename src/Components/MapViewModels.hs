{-|
Module      : Components.MapViewModels
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module Components.MapViewModels
  ( StarMapVM(..)
  , MapControlsVM(..)
  , starMapVMActive
  , starMapCenter
  , starMapSelectedStar
  , mapControlsVMUpButton
  , mapControlsVMDownButton
  , mapControlsVMLeftButton
  , mapControlsVMRightButton
  , mapControlsVMZoomButton
  )
  where

import RIO
import Control.Lens ( makeLenses )
import Components.BaseViewModels
import Components.Types
import Model


data StarMapVM = StarMapVM
  { _starMapVMActive     :: !ActiveState
  , _starMapCenter       :: !HyperCoordinates
  , _starMapSelectedStar :: !(Maybe StarId)
  } deriving (Show)


data MapControlsVM = MapControlsVM
  { _mapControlsVMUpButton    :: !ButtonVM
  , _mapControlsVMDownButton  :: !ButtonVM
  , _mapControlsVMLeftButton  :: !ButtonVM
  , _mapControlsVMRightButton :: !ButtonVM
  , _mapControlsVMZoomButton  :: !ButtonVM
  }


makeLenses ''StarMapVM
makeLenses ''MapControlsVM
