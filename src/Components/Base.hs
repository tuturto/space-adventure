{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE RankNTypes                #-}

{-|
Module      : Components.Base
Description : Base components for user interface
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental

This module contains some base components, like 'Container', 'Button' and 'Picture'.
More complex user interface elements can be found in other modules under 'Components' module.
-}
module Components.Base
  ( UIComponent(..)
  , Component(..)
  , Container(..)
  , TextDisplay(..)
  , Button(..)
  , Picture(..)
  , NoComponent(..)
  , VMSelector
  , pack
  , modalDialog
  , containerTopLeft
  , containerSize
  , containerBackground
  , containerName
  , containerList
  , containerOnClick
  , containerOnMouseDown
  , containerOnMouseUp
  , textDisplayTopLeft
  , textDisplaySize
  , textDisplayBackground
  , textDisplayText
  , textDisplayOnMouseDown
  , textDisplayOnMouseUp
  , textDisplayOnClick 
  , buttonTopLeft
  , buttonSize
  , buttonName
  , buttonTexture
  , buttonOnMouseDown
  , buttonOnMouseUp
  , buttonOnClick
  , buttonVM
  , pictureTopLeft
  , pictureSize
  , pictureName
  , pictureTexture
  , pictureOnMouseDown
  , pictureOnMouseUp
  , pictureOnClick
  , showComponent
  )
  where

import SDL
import Control.Lens
import Import
import Components.BaseViewModels
import Components.Types
import System
import UI.Types
import Graphics

data UIComponent = forall a . Component a => MkComponent a

pack :: Component a => a -> UIComponent
pack = MkComponent

instance Component UIComponent where
  draw c m (MkComponent s) = draw c m s

  doMouseDown coords model (MkComponent s) = doMouseDown coords model s

  doMouseUp coords model (MkComponent s) = doMouseUp coords model s

  doClick coords model (MkComponent s) = doClick coords model s

  topLeft (MkComponent s) = topLeft s

  size (MkComponent s) = size s

  background (MkComponent s) = background s

  name (MkComponent s) = name s

  active model (MkComponent s) = active model s

  componentAt coords (MkComponent s) = componentAt coords s

  onMouseDown (MkComponent s) = onMouseDown s

  onMouseUp (MkComponent s) = onMouseUp s

  onClick (MkComponent s) = onClick s


class Component a where
  -- |draw renders component with given coordinates being the top left corner
  draw      :: Coordinates -> Model -> a -> RIO App ()

  -- |mouseDown fires when mouse button is pressed while cursor is over the component.
  -- mouseDown also fires when mouse button is pressed while cursor is somewhere else
  -- and then cursor is moved over the component before releasing the button.
  doMouseDown   :: Coordinates -> Model -> a -> (Maybe Message, Model)
  doMouseDown coords model c =
    fireIfEnabled model coords c onMouseDown

  -- |mouseUp fires when mouse button is released while cursor is over the component.
  doMouseUp     :: Coordinates -> Model -> a -> (Maybe Message, Model)
  doMouseUp coords model c =
    fireIfEnabled model coords c onMouseUp

  -- |onClick fires when mouse button is pressed and then released over the Component.
  -- Before onClick fires, mouseUp is triggered.
  doClick     :: Coordinates -> Model -> a -> (Maybe Message, Model)
  doClick coords model c =
    fireIfEnabled model coords c onClick

  onMouseDown :: a -> Maybe Message
  onMouseDown _ = Nothing

  onMouseUp :: a -> Maybe Message
  onMouseUp _ = Nothing

  onClick :: a -> Maybe Message
  onClick _ = Nothing

  -- |top left coordinates of the component in the local coordinates
  topLeft     :: a -> Coordinates

  -- |size of the component
  size        :: a -> Size

  background :: a -> Background

  name :: a -> Text
  name _ = "Nameless"

  active :: Model -> a -> ActiveState
  active _ _ = Enabled

  -- |component at given coordinates
  -- Default implementation returns given component, if coordinates are within bounding box
  componentAt :: Coordinates -> a -> Maybe UIComponent
  componentAt (Coordinates tx ty) c =
    let Size width height = size c
        Coordinates x0 y0 = topLeft c
    in
      if (tx >= x0) && (tx <= x0 + width) && (ty >= y0) && (ty <= y0 + height)
        then
          Just $ pack c
        else
          Nothing

  {-# MINIMAL draw, topLeft, size, background, onMouseDown, onMouseUp, onClick #-}


fireIfEnabled :: Component a => Model -> Coordinates -> a -> (a -> Maybe msg) -> (Maybe msg, Model)
fireIfEnabled model _ c f =
    case active model c of
      Enabled ->
        (f c, model)

      Disabled ->
        (Nothing, model)


data Container = Container
  { _containerTopLeft     :: !Coordinates
  , _containerSize        :: !Size
  , _containerBackground  :: !Background
  , _containerName        :: !Text
  , _containerList        :: ![UIComponent]
  , _containerOnClick     :: !(Maybe Message)
  , _containerOnMouseDown :: !(Maybe Message)
  , _containerOnMouseUp   :: !(Maybe Message)
  }

instance Component Container where
  draw offset m c = do
    renderer <- asks appRenderer
    let globalCoords = addCoords offset (_containerTopLeft c)
    renderBackground renderer globalCoords (_containerSize c) (_containerBackground c)
    mapM_ (draw globalCoords m) (reverse $ _containerList c)

  topLeft = _containerTopLeft

  size = _containerSize

  background = _containerBackground

  name = _containerName

  componentAt (Coordinates tx ty) c =
    let Size width height = size c
        Coordinates x0 y0 = topLeft c
        match = listToMaybe $ mapMaybe (componentAt (Coordinates (tx - x0) (ty - y0))) $ _containerList c
    in
      case match of
        Just _ ->
          match

        Nothing ->
          if (tx >= x0) && (tx <= x0 + width) && (ty >= y0) && (ty <= y0 + height)
            then
              Just $ pack c
            else
              Nothing

  onClick = _containerOnClick

  onMouseDown = _containerOnMouseDown

  onMouseUp = _containerOnMouseUp


data Picture = Picture
  { _pictureTopLeft     :: !Coordinates
  , _pictureSize        :: !Size
  , _pictureName        :: !Text
  , _pictureTexture     :: !Texture
  , _pictureOnMouseDown :: !(Maybe Message)
  , _pictureOnMouseUp   :: !(Maybe Message)
  , _pictureOnClick     :: !(Maybe Message)
  }

instance Component Picture where
  draw coords _ p = do
    renderer <- asks appRenderer
    let Coordinates x y = addCoords coords (_pictureTopLeft p)
    let tex = _pictureTexture p
    let Size w h = _pictureSize p
    copy renderer tex Nothing
            (Just (Rectangle (P $ V2 (fromIntegral x) (fromIntegral y))
            (V2 (fromIntegral w) (fromIntegral h))))

  topLeft = _pictureTopLeft

  size = _pictureSize

  background _ = TransparentBackground

  name = _pictureName

  onMouseDown = _pictureOnMouseDown

  onMouseUp = _pictureOnMouseUp

  onClick = _pictureOnClick


data Button = Button
  { _buttonTopLeft     :: !Coordinates
  , _buttonSize        :: !Size
  , _buttonName        :: !Text
  , _buttonTexture     :: !ButtonTexture
  , _buttonOnMouseDown :: !(Maybe Message)
  , _buttonOnMouseUp   :: !(Maybe Message)
  , _buttonOnClick     :: !(Maybe Message)
  , _buttonVM          :: !(VMSelector ButtonVM)
  }

instance Component Button where
  draw (Coordinates x y) m b = do
    renderer <- asks appRenderer
    let Coordinates bx by = _buttonTopLeft b
        Size w h = _buttonSize b
        (vmGet, _) = _buttonVM b
        vm = vmGet m
        texture = case (_buttonVMState <$> vm, _buttonVMActive <$> vm) of
                    (_, Just Disabled) ->
                      _buttonTextureDisabled $ _buttonTexture b

                    (Just ButtonUp, _) ->
                      _buttonTextureEnabledUp $ _buttonTexture b

                    (Just ButtonDown, _) ->
                      _buttonTextureEnabledDown $ _buttonTexture b

                    (Just ButtonPressed, _) ->
                      _buttonTextureEnabledDown $ _buttonTexture b

                    (_, _) ->
                      _buttonTextureDisabled $ _buttonTexture b

    copy renderer texture Nothing 
          (Just (Rectangle (P $ V2 (fromIntegral $ bx + x) (fromIntegral $ by + y)) 
                (V2 (fromIntegral w) (fromIntegral h))))

    return ()

  topLeft = _buttonTopLeft

  size = _buttonSize

  background _ = TransparentBackground

  name = _buttonName

  active model b =
    maybe Disabled _buttonVMActive (bget model)
      where
        (bget, _) = _buttonVM b


  doMouseDown _ model b =
    case active model b of
      Enabled ->
        let newModel = case vm of
                    Nothing ->
                      model
                    Just bvm ->
                      vmSet model (bvm { _buttonVMState = ButtonDown})
            vm = vmGet model
            (vmGet, vmSet) = _buttonVM b
        in
          (onMouseDown b, newModel)
      
      Disabled ->
        (Nothing, model)


  doMouseUp _ model b =
    case active model b of
      Enabled ->
        let newModel = case vm of
                    Nothing ->
                      model
                    Just bvm ->
                      vmSet model (bvm { _buttonVMState = ButtonUp})
            vm = vmGet model
            (vmGet, vmSet) = _buttonVM b
        in
          (onMouseUp b, newModel)

      Disabled ->
        (Nothing, model)

  onMouseDown = _buttonOnMouseDown

  onMouseUp = _buttonOnMouseUp

  onClick = _buttonOnClick


data TextDisplay = TextDisplay
  { _textDisplayTopLeft     :: !Coordinates
  , _textDisplaySize        :: !Size
  , _textDisplayBackground  :: !Background
  , _textDisplayText        :: !Text
  , _textDisplayOnMouseDown :: !(Maybe Message)
  , _textDisplayOnMouseUp   :: !(Maybe Message)
  , _textDisplayOnClick     :: !(Maybe Message)
  }

instance Component TextDisplay where
  draw = undefined

  topLeft = _textDisplayTopLeft

  size = _textDisplaySize

  background = _textDisplayBackground

  onMouseDown = _textDisplayOnMouseDown

  onMouseUp = _textDisplayOnMouseUp

  onClick = _textDisplayOnClick


data NoComponent = NoComponent

instance Component NoComponent where
  draw _ _ _ = return ()
  topLeft _ = Coordinates 0 0
  size _ = Size 0 0
  background _ = TransparentBackground
  onMouseDown _ = Nothing
  onMouseUp _ = Nothing
  onClick _ = Nothing


showComponent :: Bool -> UIComponent -> UIComponent
showComponent True c =
  c

showComponent False _ =
  pack $ NoComponent


type VMSelector a = ( Model -> Maybe a
                    , Model -> a -> Model
                    )


-- |Create a modal dialog with given content
-- When user clicks outside of the dialog, given message is raised.
-- This can be handy when creating a dialog that should close when user clicks
-- outside of it.
modalDialog :: Maybe Message -> Container -> UIComponent
modalDialog msg c = 
  pack $ Container
    { _containerTopLeft = Coordinates 0 0
    , _containerSize = Size 640 480
    , _containerName = "Modal dialog"
    , _containerBackground = TransparentBackground -- TODO: shading maybe?
    , _containerOnClick = msg
    , _containerOnMouseDown = Nothing
    , _containerOnMouseUp = Nothing
    , _containerList = [ pack c ]
    }


makeLenses ''Container
makeLenses ''TextDisplay
makeLenses ''Button
makeLenses ''Picture
