{-|
Module      : Components.Types
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module Components.Types
  ( ActiveState(..)
  , Background(..)
  , ButtonColour(..)
  , ButtonState(..)
  , ButtonTexture(..)
  , Freeable(..) 
  , MapControlsTextures(..)
  , MapInfoPanelTextures(..)  
  , StarMapMessage(..)
  , StarmapTextures(..)
  , Symbol(..)
  , SymbolColour(..)
  , TextColour(..)
  , _ButtonDown
  , _ButtonUp
  , _ButtonPressed
  , _Enabled
  , _Disabled
  , _SolidColourBackground
  , _BackgroundTexture
  , _TransparentBackground  
  , buttonTextureEnabledUp
  , buttonTextureEnabledDown
  , buttonTextureDisabled
  , mapControlsTexturesBackground
  , mapControlsTexturesUp
  , mapControlsTexturesDown
  , mapControlsTexturesLeft
  , mapControlsTexturesRight
  , mapControlsTexturesZoom
  , starMapTexturesBackground
  , starmapTexturesInfoPanel
  , starmapTexturesControls
  , starMapTexturesStars
  )
  where

import RIO
import Control.Lens ( makePrisms, makeLenses )
import SDL


class Freeable a where  
  free :: MonadIO m => a -> m ()


instance Freeable Texture where
  free = destroyTexture

instance Freeable Surface where
  free = freeSurface


data ButtonTexture = ButtonTexture
  { _buttonTextureEnabledUp    :: !Texture
  , _buttonTextureEnabledDown  :: !Texture
  , _buttonTextureDisabled     :: !Texture
  }


instance Freeable ButtonTexture where
  free t = do
    free $ _buttonTextureEnabledUp t
    free $ _buttonTextureEnabledDown t
    free $ _buttonTextureDisabled t


data ButtonColour =
  RedButton
  | GreenButton
  | BlueButton
  | GreyButton
  deriving (Show, Read, Eq)


data TextColour =
  RedText
  | GreenText
  | BlueText
  | GreyText
  deriving (Show, Read, Eq)


data SymbolColour =
  RedSymbol
  | GreenSymbol
  | BlueSymbol
  | GreySymbol
  deriving (Show, Read, Eq)


data Symbol =
  SymbolPlus
  | SymbolMinus
  | SymbolLeftArrow
  | SymbolRightArrow
  | SymbolUpArrow
  | SymbolDownArrow
  | SymbolCWArrow
  | SymbolCCWArrow
  | SymbolX
  deriving (Eq)


data ButtonState =
  ButtonDown
  | ButtonUp
  | ButtonPressed
  deriving (Eq, Show)


data ActiveState =
  Enabled
  | Disabled
  deriving (Eq, Show)


data Background =
  SolidColourBackground (V4 Word8)
  | BackgroundTexture Texture
  | TransparentBackground


data StarMapMessage =
  StarMapUpClicked
  | StarMapDownClicked
  | StarMapLeftClicked
  | StarMapRightClicked
  deriving (Show, Read, Eq)


data StarmapTextures = StarmapTextures
  { _starmapTexturesInfoPanel  :: !MapInfoPanelTextures
  , _starmapTexturesControls   :: !MapControlsTextures
  , _starMapTexturesBackground :: !Texture
  , _starMapTexturesStars      :: !Texture
  }

instance Freeable StarmapTextures where
  free t = do
    free $ _starmapTexturesInfoPanel t
    free $ _starmapTexturesControls t
    free $ _starMapTexturesBackground t
    free $ _starMapTexturesStars t


data MapInfoPanelTextures = MapInfoPanelTextures
  { 

  }

instance Freeable MapInfoPanelTextures where
  free _ = return ()


data MapControlsTextures = MapControlsTextures
  { _mapControlsTexturesBackground :: !Texture
  , _mapControlsTexturesUp         :: !ButtonTexture
  , _mapControlsTexturesDown       :: !ButtonTexture
  , _mapControlsTexturesLeft       :: !ButtonTexture
  , _mapControlsTexturesRight      :: !ButtonTexture
  , _mapControlsTexturesZoom       :: !ButtonTexture
  }

instance Freeable MapControlsTextures where
  free t = do
    free $ _mapControlsTexturesBackground t
    free $ _mapControlsTexturesUp t
    free $ _mapControlsTexturesDown t
    free $ _mapControlsTexturesLeft t
    free $ _mapControlsTexturesRight t
    free $ _mapControlsTexturesZoom t


makePrisms ''ActiveState
makePrisms ''Background
makePrisms ''ButtonState

makeLenses ''ButtonTexture
makeLenses ''StarmapTextures
makeLenses ''MapInfoPanelTextures
makeLenses ''MapControlsTextures
