{-|
Module      : Components.BaseViewModels
Description : View models for base components for user interface
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental

This module contains view models for some base components defined in 'Components.Base'.
-}
module Components.BaseViewModels
  ( ButtonVM(..)
  , buttonVMState
  , buttonVMActive
  , enabledButton
  , disabledButton
  )
  where

import RIO
import Control.Lens ( makeLenses )
import Components.Types

data ButtonVM = ButtonVM
  { _buttonVMState       :: !ButtonState
  , _buttonVMActive      :: !ActiveState
  } deriving (Show)


enabledButton :: ButtonVM
enabledButton = 
  ButtonVM
    { _buttonVMState   = ButtonUp
    , _buttonVMActive  = Enabled
    }


disabledButton :: ButtonVM
disabledButton = 
  ButtonVM
    { _buttonVMState   = ButtonUp
    , _buttonVMActive  = Disabled
    }

makeLenses ''ButtonVM
