{-|
Module      : Transitions
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module Transitions 
  ( newGame
  , mainMenu
  ) where


import Import
import Control.Lens ( folded )
import Control.Monad.Random.Strict
import Generators.Space
import Model
import System
import UI.MainMenu.Model
import UI.SpaceStation.Model
import Components.BaseViewModels
import Components.MapViewModels
import Components.StarfieldViewModels
import Components.Types


-- | Move into main menu
mainMenu :: RandomGen g => Tick -> Rand g Model
mainMenu t = do
  sf <- initializeStarField 200 (640, 480) (10, 50)
  return $ GameInMainMenu t
                 MainMenuModel 
                    { _mainMenuModelNewGameButton = enabledButton
                    , _mainMenuModelLoadButton = disabledButton
                    , _mainMenuModelOptionsButton = disabledButton
                    , _mainMenuModelQuitButton = enabledButton
                    , _mainMenuModelOkButton = enabledButton
                    , _mainMenuModelCancelButton = enabledButton
                    , _mainMenuModelQuitConfirmationShown = False
                    , _mainMenuModelStarfieldVM = sf
                    }


-- | Start a new game
-- 'Types.Tick' is current tick of the program
newGame :: RandomGen g => AppConfig -> Tick -> Rand g Model
newGame c t = do
  space <- (generateSpace c)
  let 
    mapVM = StarMapVM 
      { _starMapVMActive     = Enabled
      , _starMapCenter       = mapCenter
      , _starMapSelectedStar = Nothing
      }
    controlsVM = MapControlsVM
      { _mapControlsVMUpButton    = enabledButton
      , _mapControlsVMDownButton  = enabledButton
      , _mapControlsVMLeftButton  = enabledButton
      , _mapControlsVMRightButton = enabledButton
      , _mapControlsVMZoomButton  = enabledButton
      }
    model = SpaceStationModel
      { _spaceStationModelDockButton            = enabledButton
      , _spaceStationModelMapButton             = enabledButton
      , _spaceStationModelMarketButton          = enabledButton
      , _spaceStationModelJobsButton            = enabledButton
      , _spaceStationModelNewsButton            = enabledButton
      , _spaceStationModelXButton               = enabledButton
      , _spaceStationModelOkButton              = enabledButton
      , _spaceStationModelCancelButton          = enabledButton
      , _spaceStationModelQuitConfirmationShown = False
      , _spaceStationModelCurrentView           = MapView
      , _spaceStationModelStarMap               = mapVM
      , _spaceStationModelMapControls           = controlsVM
      , _spaceStationModelTick                  = t
      }
    universe = GameData 
      { _gameDataSystems = space
      }
    station = space ^? folded 
                      . starSystemSpaceStations . folded
    sIdm = _spaceStationId <$> station

    -- calculate where star map display should be centered
    -- ideally it should center on the starting location
    -- if the starting location is close to the edge of the galaxy, move view so that the edge is not visible
    -- this way, the star map always shows full view of stars and not empty space
    (gw, gh) = c ^. appConfigGalaxySize
    (smw, smh) = (628, 440) -- Size of the starmap display - border
    galaxyCenter = HyperCoordinates (divInt gw 2) (divInt gh 2)
    (HyperCoordinates mx my) = 
      case station of
        Nothing -> 
          galaxyCenter

        Just s ->
          fromMaybe galaxyCenter $
                    (_starSystemLocation <$> (spaceStation'sSystem space s))

    mapCenter = HyperCoordinates (clamp mx (smw `divInt` 2, gw - smw `divInt` 2))
                                 (clamp my (smh `divInt` 2, gh - smh `divInt` 2))
    
  return $ DockedInSpaceStation t model (fromMaybe undefined sIdm) universe
