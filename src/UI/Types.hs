{-|
Module      : UI.Types
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module UI.Types
  ( Textures (..)
  , Message (..)
  , MainMenuMessage(..)
  , SpaceStationMessage(..)
  , ButtonTemplate(..)
  , DisabledButtonTemplate(..)
  , ButtonSymbols(..)
  , UITextures(..)
  , MainMenuTextures(..)
  , Freeable(..)
  , SpaceStationTextures(..)
  , Coordinates(..)
  , Size(..)
  , ActiveState(..)
  , Background(..)
  , buttonVMState
  , buttonVMActive
  , _ButtonDown
  , _ButtonUp
  , _ButtonPressed
  , _Enabled
  , _Disabled
  , _SolidColourBackground
  , _BackgroundTexture
  , _TransparentBackground
  , addCoords
  , _Coordinates
  , _Size
  , texturesRedButton
  , texturesGreenButton
  , texturesBlueButton
  , texturesGreyButton
  , texturesDisabledButton
  , texturesInset
  , texturesBevel
  , texturesRidge
  , texturesRedText
  , texturesBlueText
  , texturesGreenText
  , texturesGreyText
  , texturesDisabledText
  , texturesSmallStar
  , texturesStar1
  , buttonTemplateActive
  , buttonTemplateDefault
  , buttonTemplateSymbols
  , disabledButtonTemplateButton
  , disabledButtonTemplateSymbols
  , buttonSymbolsPlus
  , buttonSymbolsMinus
  , buttonSymbolsLeftArrow
  , buttonSymbolsRightArrow
  , buttonSymbolsUpArrow
  , buttonSymbolsDownArrow
  , buttonSymbolsCWArrow
  , buttonSymbolsCWWArrow
  , buttonSymbolsX
  , mainMenuTexturesNewGame
  , mainMenuTexturesLoad
  , mainMenuTexturesOptions
  , mainMenuTexturesQuit
  , mainMenuTexturesOk
  , mainMenuTexturesCancel
  , mainMenuTexturesContainer
  , mainMenuTexturesDialog
  , mainMenuTexturesConfirmationText
  , spaceStationTexturesMainContainer
  , spaceStationTexturesInfoContainer
  , spaceStationTexturesDockButton
  , spaceStationTexturesMapButton
  , spaceStationTexturesMarketkButton
  , spaceStationTexturesJobsButton
  , spaceStationTexturesNewsButton
  , spaceStationTexturesXButton
  , spaceStationTexturesInfoBar
  , spaceStationTexturesOkButton
  , spaceStationTexturesCancelButton
  , spaceStationTexturesConfirmationText
  , spaceStationTexturesQuitDialog
  , spaceStationTexturesStarMap
  , spaceStationTexturesControls
  ) where

import RIO

import SDL 
import Control.Lens.TH
import Components.BaseViewModels
import Components.Types


data Textures = Textures
  { _texturesRidge             :: !Surface
  , _texturesBevel             :: !Surface
  , _texturesInset             :: !Surface
  , _texturesRedButton         :: !ButtonTemplate
  , _texturesGreenButton       :: !ButtonTemplate
  , _texturesBlueButton        :: !ButtonTemplate
  , _texturesGreyButton        :: !ButtonTemplate
  , _texturesDisabledButton    :: !DisabledButtonTemplate
  , _texturesRedText           :: !Surface
  , _texturesBlueText          :: !Surface
  , _texturesGreenText         :: !Surface
  , _texturesGreyText          :: !Surface
  , _texturesDisabledText      :: !Surface
  , _texturesSmallStar         :: !Surface
  , _texturesStar1             :: !Texture
  }

instance Freeable Textures where
  free t = do
    free $ _texturesRidge t
    free $ _texturesBevel t
    free $ _texturesInset t
    free $ _texturesRedText t
    free $ _texturesGreenText t
    free $ _texturesGreyText t
    free $ _texturesDisabledText t
    free $ _texturesRedButton t
    free $ _texturesGreenButton t
    free $ _texturesBlueButton t
    free $ _texturesGreyButton t
    free $ _texturesDisabledButton t
    free $ _texturesSmallStar t
    free $ _texturesStar1 t


data UITextures =
  MainMenuUITextures MainMenuTextures
  | SpaceStationUITextures SpaceStationTextures
  | NoUITextures

instance Freeable UITextures where
  free t =
    case t of
      MainMenuUITextures txt ->
        free txt

      SpaceStationUITextures txt ->
        free txt

      NoUITextures ->
        return ()


data MainMenuTextures = MainMenuTextures
  { _mainMenuTexturesNewGame          :: !ButtonTexture
  , _mainMenuTexturesLoad             :: !ButtonTexture
  , _mainMenuTexturesOptions          :: !ButtonTexture
  , _mainMenuTexturesQuit             :: !ButtonTexture
  , _mainMenuTexturesOk               :: !ButtonTexture
  , _mainMenuTexturesCancel           :: !ButtonTexture
  , _mainMenuTexturesConfirmationText :: !Texture
  , _mainMenuTexturesContainer        :: !Texture
  , _mainMenuTexturesDialog           :: !Texture
  }

instance Freeable MainMenuTextures where
  free t = do
    free $ _mainMenuTexturesNewGame t
    free $ _mainMenuTexturesLoad t
    free $ _mainMenuTexturesOptions t
    free $ _mainMenuTexturesQuit t
    free $ _mainMenuTexturesOk t
    free $ _mainMenuTexturesCancel t
    free $ _mainMenuTexturesContainer t
    free $ _mainMenuTexturesDialog t


data SpaceStationTextures = SpaceStationTextures
  { _spaceStationTexturesMainContainer    :: !Texture
  , _spaceStationTexturesInfoContainer    :: !Texture
  , _spaceStationTexturesQuitDialog       :: !Texture
  , _spaceStationTexturesDockButton       :: !ButtonTexture
  , _spaceStationTexturesMapButton        :: !ButtonTexture
  , _spaceStationTexturesMarketkButton    :: !ButtonTexture
  , _spaceStationTexturesJobsButton       :: !ButtonTexture
  , _spaceStationTexturesNewsButton       :: !ButtonTexture
  , _spaceStationTexturesXButton          :: !ButtonTexture
  , _spaceStationTexturesOkButton         :: !ButtonTexture
  , _spaceStationTexturesCancelButton     :: !ButtonTexture
  , _spaceStationTexturesInfoBar          :: !Texture
  , _spaceStationTexturesConfirmationText :: !Texture
  , _spaceStationTexturesStarMap          :: !StarmapTextures
  , _spaceStationTexturesControls         :: !MapControlsTextures
  }

instance Freeable SpaceStationTextures where
  free t = do
    free $ _spaceStationTexturesMainContainer t
    free $ _spaceStationTexturesInfoContainer t
    free $ _spaceStationTexturesQuitDialog t
    free $ _spaceStationTexturesDockButton t
    free $ _spaceStationTexturesMapButton t
    free $ _spaceStationTexturesMarketkButton t
    free $ _spaceStationTexturesJobsButton t
    free $ _spaceStationTexturesNewsButton t
    free $ _spaceStationTexturesXButton t
    free $ _spaceStationTexturesOkButton t
    free $ _spaceStationTexturesCancelButton t
    free $ _spaceStationTexturesInfoBar t
    free $ _spaceStationTexturesConfirmationText t
    free $ _spaceStationTexturesStarMap t
    free $ _spaceStationTexturesControls t


data ButtonTemplate = ButtonTemplate
  { _buttonTemplateActive   :: !Surface
  , _buttonTemplateDefault  :: !Surface
  , _buttonTemplateSymbols  :: !ButtonSymbols
  }

instance Freeable ButtonTemplate where
  free t = do
    free $ _buttonTemplateActive t
    free $ _buttonTemplateDefault t
    free $ _buttonTemplateSymbols t


data DisabledButtonTemplate = DisabledButtonTemplate
  { _disabledButtonTemplateButton  :: !Surface
  , _disabledButtonTemplateSymbols :: !ButtonSymbols
  }

instance Freeable DisabledButtonTemplate where
  free t = do
    free $ _disabledButtonTemplateButton t
    free $ _disabledButtonTemplateSymbols t


data ButtonSymbols = ButtonSymbols
  { _buttonSymbolsPlus       :: !Surface
  , _buttonSymbolsMinus      :: !Surface
  , _buttonSymbolsLeftArrow  :: !Surface
  , _buttonSymbolsRightArrow :: !Surface
  , _buttonSymbolsUpArrow    :: !Surface
  , _buttonSymbolsDownArrow  :: !Surface
  , _buttonSymbolsCWArrow    :: !Surface
  , _buttonSymbolsCWWArrow   :: !Surface
  , _buttonSymbolsX          :: !Surface
  }

instance Freeable ButtonSymbols where
  free t = do
    free $ _buttonSymbolsPlus t
    free $ _buttonSymbolsMinus t
    free $ _buttonSymbolsLeftArrow t
    free $ _buttonSymbolsRightArrow t
    free $ _buttonSymbolsUpArrow t
    free $ _buttonSymbolsDownArrow t
    free $ _buttonSymbolsCWArrow t
    free $ _buttonSymbolsCWWArrow t
    free $ _buttonSymbolsX t


-- |Domain messages indicate that user has performed an action and the system
-- should react to it some way.
data Message = 
  MainMenuMessage MainMenuMessage
  | SpaceStationMessage SpaceStationMessage
  | QuitRequested
  deriving (Show, Read, Eq)


data SpaceStationMessage =
  SSQuitClicked
  | SSQuitConfirmed
  | SSQuitCancelled
  | SSDockClicked
  | SSMapClicked
  | SSMarketClicked
  | SSJobsClicked
  | SSNewsClicked
  deriving (Show, Read, Eq)


-- |Messages for main menu
data MainMenuMessage =
  QuitClicked
  | QuitConfirmed
  | QuitCancelled
  | NewGameRequested
  deriving (Show, Read, Eq)


data Coordinates = Coordinates !Int !Int


addCoords :: Coordinates -> Coordinates -> Coordinates
addCoords (Coordinates x0 y0) (Coordinates x1 y1) =
  Coordinates (x0 + x1) (y0 + y1)


data Size = Size !Int !Int
  deriving Show


makeLenses ''Textures
makeLenses ''ButtonSymbols
makeLenses ''ButtonTemplate
makeLenses ''DisabledButtonTemplate
makeLenses ''MainMenuTextures
makeLenses ''SpaceStationTextures
makePrisms ''Coordinates
makePrisms ''Size
