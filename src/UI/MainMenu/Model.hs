{-|
Module      : UI.MainMenu.Model
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module UI.MainMenu.Model 
  ( MainMenuModel(..)
  , mainMenuModelNewGameButton
  , mainMenuModelLoadButton
  , mainMenuModelOptionsButton
  , mainMenuModelQuitButton
  , mainMenuModelQuitConfirmationShown
  , mainMenuModelCancelButton
  , mainMenuModelOkButton
  , mainMenuModelStarfieldVM
  ) where

import RIO
import Control.Lens.TH (makeLenses)
import Components.BaseViewModels
import Components.StarfieldViewModels

-- |State of the game
data MainMenuModel = MainMenuModel
  { _mainMenuModelNewGameButton         :: !ButtonVM
  , _mainMenuModelLoadButton            :: !ButtonVM
  , _mainMenuModelOptionsButton         :: !ButtonVM
  , _mainMenuModelQuitButton            :: !ButtonVM
  , _mainMenuModelOkButton              :: !ButtonVM
  , _mainMenuModelCancelButton          :: !ButtonVM
  , _mainMenuModelQuitConfirmationShown :: !Bool
  , _mainMenuModelStarfieldVM           :: !StarfieldVM
  }

makeLenses ''MainMenuModel
