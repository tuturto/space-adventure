{-# LANGUAGE RankNTypes #-}

{-|
Module      : UI.MainMenu.View
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module UI.MainMenu.View
  ( uiView
  , update
  , stepTime
  ) where

import Control.Monad.Random.Strict
import Control.Lens ( _1, _2 )
import Components.Base
import Components.Starfield
import qualified Components.StarfieldViewModels as SF
import Components.Types
import UI.MainMenu.Model
import UI.Types
import System
import Import
import Graphics 
import Colours
import Transitions

uiView :: UITextures -> MainMenuModel -> RIO App (Container, UITextures)
uiView (MainMenuUITextures t) mm = do
  return 
    ( Container
      { _containerTopLeft = Coordinates 0 0
      , _containerSize = Size 640 480
      , _containerName = "Base container"
      , _containerBackground = SolidColourBackground Colours.black
      , _containerOnClick = Nothing
      , _containerOnMouseDown = Nothing
      , _containerOnMouseUp = Nothing
      , _containerList =
          [ showComponent (mm ^. mainMenuModelQuitConfirmationShown) 
              (modalDialog Nothing $
                Container
                  { _containerTopLeft = Coordinates 250 213
                  , _containerSize = Size 141 54
                  , _containerName = "Confirmation dialog"
                  , _containerBackground = BackgroundTexture $ t ^. mainMenuTexturesDialog
                  , _containerOnClick = Nothing
                  , _containerOnMouseDown = Nothing
                  , _containerOnMouseUp = Nothing
                  , _containerList =
                      [ pack $ Picture
                          { _pictureTopLeft     = Coordinates 35 15
                          , _pictureSize        = Size 110 16
                          , _pictureName        = "Confirmation text"
                          , _pictureTexture     = t ^. mainMenuTexturesConfirmationText
                          , _pictureOnMouseDown = Nothing
                          , _pictureOnMouseUp   = Nothing
                          , _pictureOnClick     = Nothing
                          }
                      , pack $ Button
                          { _buttonTopLeft     = Coordinates 26 31
                          , _buttonSize        = Size 52 18
                          , _buttonName        = "Ok button"
                          , _buttonTexture     = t ^. mainMenuTexturesOk
                          , _buttonOnMouseDown = Nothing
                          , _buttonOnMouseUp   = Nothing
                          , _buttonOnClick     = Just $ MainMenuMessage QuitConfirmed
                          , _buttonVM          = accessor mainMenuModelOkButton
                          }
                      , pack $ Button
                          { _buttonTopLeft     = Coordinates 82 31
                          , _buttonSize        = Size 52 18
                          , _buttonName        = "Cancel button"
                          , _buttonTexture     = t ^. mainMenuTexturesCancel
                          , _buttonOnMouseDown = Nothing
                          , _buttonOnMouseUp   = Nothing
                          , _buttonOnClick     = Just $ MainMenuMessage QuitCancelled
                          , _buttonVM          = accessor mainMenuModelCancelButton
                          }
                      ]
                  })
          , pack $ 
              Container
                { _containerTopLeft = Coordinates 272 176
                , _containerSize = Size 96 128
                , _containerName = "Main menu"
                , _containerBackground = BackgroundTexture $ t ^. mainMenuTexturesContainer
                , _containerOnClick = Nothing
                , _containerOnMouseDown = Nothing
                , _containerOnMouseUp = Nothing
                , _containerList =
                    [ pack $ 
                        Button
                          { _buttonTopLeft     = Coordinates 22 19
                          , _buttonSize        = Size 52 18
                          , _buttonName        = "New game button"
                          , _buttonTexture     = t ^. mainMenuTexturesNewGame
                          , _buttonOnMouseDown = Nothing
                          , _buttonOnMouseUp   = Nothing
                          , _buttonOnClick     = Just $ MainMenuMessage NewGameRequested
                          , _buttonVM          = accessor mainMenuModelNewGameButton
                          }
                    , pack $ 
                        Button
                          { _buttonTopLeft     = Coordinates 22 43
                          , _buttonSize        = Size 52 18
                          , _buttonName        = "Load game button"
                          , _buttonTexture     = t ^. mainMenuTexturesLoad
                          , _buttonOnMouseDown = Nothing
                          , _buttonOnMouseUp   = Nothing
                          , _buttonOnClick     = Nothing
                          , _buttonVM          = accessor mainMenuModelLoadButton
                          }
                    , pack $ 
                        Button
                          { _buttonTopLeft     = Coordinates 22 67
                          , _buttonSize        = Size 52 18
                          , _buttonName        = "Options button"
                          , _buttonTexture     = t ^. mainMenuTexturesOptions
                          , _buttonOnMouseDown = Nothing
                          , _buttonOnMouseUp   = Nothing
                          , _buttonOnClick     = Nothing
                          , _buttonVM          = accessor mainMenuModelOptionsButton
                          }
                    , pack $ 
                        Button
                          { _buttonTopLeft     = Coordinates 22 91
                          , _buttonSize        = Size 52 18
                          , _buttonName        = "Quit button"
                          , _buttonTexture     = t ^. mainMenuTexturesQuit
                          , _buttonOnMouseDown = Nothing
                          , _buttonOnMouseUp   = Nothing
                          , _buttonOnClick     = Just $ MainMenuMessage QuitClicked
                          , _buttonVM          = accessor mainMenuModelQuitButton
                          }
                    ]
                }
          , pack $
              Starfield
                { _starfieldTopLeft    = Coordinates 0 0
                , _starfieldSize       = Size 640 480
                , _starfieldName       = "starfield"
                , _starfieldBackground = SolidColourBackground Colours.black
                , _starfieldVM         = accessor mainMenuModelStarfieldVM
                }
          ]
      }
    , MainMenuUITextures t
    )


uiView t mm = do
  logDebug "freeing textures..."
  free t
  logDebug "generating main menu textures..."
  textures <- asks appTextures
  renderer <- asks appRenderer

  newBtn <- createButtonTexture renderer textures (Just "new") Nothing BlueButton (Size 50 18) 
  loadBtn <- createButtonTexture renderer textures (Just "load") Nothing BlueButton (Size 50 18)
  optionsBtn <- createButtonTexture renderer textures (Just "options") Nothing BlueButton (Size 50 18)
  quitBtn <- createButtonTexture renderer textures (Just "quit") Nothing BlueButton (Size 50 18)

  okBtn <- createButtonTexture renderer textures (Just "ok") Nothing BlueButton (Size 50 18)
  cancelBtn <- createButtonTexture renderer textures (Just "cancel") Nothing BlueButton (Size 50 18)

  mainContainer <- createDialogTexture renderer textures $ Size 96 128
  dialog <- createDialogTexture renderer textures $ Size 141 54

  confirmation <- createText renderer textures GreyText Enabled (Size 110 16) "are you sure?"

  let txt = 
        MainMenuTextures
          { _mainMenuTexturesNewGame           = newBtn
          , _mainMenuTexturesLoad              = loadBtn
          , _mainMenuTexturesOptions           = optionsBtn
          , _mainMenuTexturesQuit              = quitBtn
          , _mainMenuTexturesOk                = okBtn
          , _mainMenuTexturesCancel            = cancelBtn
          , _mainMenuTexturesContainer         = mainContainer
          , _mainMenuTexturesDialog            = dialog
          , _mainMenuTexturesConfirmationText  = confirmation
          }

  logDebug "generating main menu..."
  uiView (MainMenuUITextures txt) mm


-- |Build a handy accessor that can be used to get and set a specific field inside MainMenuModel
accessor :: Lens' MainMenuModel a -> VMSelector a
accessor f =
  ( \m ->
      m ^? _GameInMainMenu . _2 . f
  , \m vm ->
      m & _GameInMainMenu . _2 . f .~ vm
  )


update :: RandomGen g => AppConfig -> Tick -> Model -> MainMenuMessage -> Rand g Model
update c t model msg = 
  case msg of
    QuitClicked ->
      return $
        model & _GameInMainMenu . _2 . mainMenuModelQuitConfirmationShown .~ True

    QuitConfirmed ->
      return $ GameExiting t

    QuitCancelled ->
      return $
        model & _GameInMainMenu . _2 . mainMenuModelQuitConfirmationShown .~ False

    NewGameRequested ->
      newGame c t


stepTime :: RandomGen g => Model -> Tick -> Tick -> Rand g Model
stepTime model ot nt = do
  let
    vm = model ^? _GameInMainMenu . _2 .  mainMenuModelStarfieldVM 
  case vm of
        Just stars -> do
          ns <- SF.stepTime stars ot nt
          return $ model & _GameInMainMenu . _2 . mainMenuModelStarfieldVM .~ ns
                         & _GameInMainMenu . _1 .~ nt
          
        Nothing ->
          return model
