{-|
Module      : UI.SpaceStation.Model
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module UI.SpaceStation.Model 
  ( SpaceStationModel(..)
  , SpaceStationView(..)
  , _DockView
  , _MapView
  , _MarketView
  , _JobsView
  , _NewsView
  , mapViewModelStarMap
  , spaceStationModelDockButton
  , spaceStationModelMapButton
  , spaceStationModelMarketButton
  , spaceStationModelJobsButton
  , spaceStationModelNewsButton
  , spaceStationModelXButton
  , spaceStationModelQuitConfirmationShown
  , spaceStationModelOkButton
  , spaceStationModelCancelButton
  , spaceStationModelCurrentView
  , spaceStationModelStarMap
  , spaceStationModelMapControls
  , spaceStationModelTick
  )
  where

import Import
import Control.Lens.TH ( makeLenses, makePrisms )
import Components.BaseViewModels
import Components.MapViewModels


-- |State of the UI in space station
data SpaceStationModel = SpaceStationModel
  { _spaceStationModelDockButton            :: !ButtonVM
  , _spaceStationModelMapButton             :: !ButtonVM
  , _spaceStationModelMarketButton          :: !ButtonVM
  , _spaceStationModelJobsButton            :: !ButtonVM
  , _spaceStationModelNewsButton            :: !ButtonVM
  , _spaceStationModelXButton               :: !ButtonVM
  , _spaceStationModelOkButton              :: !ButtonVM
  , _spaceStationModelCancelButton          :: !ButtonVM
  , _spaceStationModelQuitConfirmationShown :: !Bool
  , _spaceStationModelCurrentView           :: !SpaceStationView
  , _spaceStationModelStarMap               :: !StarMapVM
  , _spaceStationModelMapControls           :: !MapControlsVM
  , _spaceStationModelTick                  :: !Tick
  }


data SpaceStationView =
  DockView
  | MapView
  | MarketView
  | JobsView
  | NewsView
  deriving (Show, Read, Eq, Enum, Bounded, Ord)


data MapViewModel = MapViewModel
  { _mapViewModelStarMap :: !StarMapVM
  }

makePrisms ''SpaceStationView

makeLenses ''MapViewModel
makeLenses ''SpaceStationModel
