{-# LANGUAGE RankNTypes        #-}

{-|
Module      : UI.SpaceStation.View
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module UI.SpaceStation.View
  ( uiView
  , update
  ) where

import Import
import Control.Monad.Random.Strict
import Control.Lens
  ( _2
  , folded  
  , filtered  
  )
import Colours
import Graphics
import System
import Model
import Transitions
import UI.SpaceStation.Model
import UI.Types
import Components.Types
import Components.Base
import Components.Map
import Components.MapViewModels



-- | 'Components.Base.Container' containing the user interface definition that should be shown, coupled
-- with the necessary textures needed to show it.
uiView :: UITextures -> SpaceStationModel -> SpaceStationId -> GameData -> RIO App (Container, UITextures)
uiView st@(SpaceStationUITextures t) sm sId gd = do
  return 
    ( Container
      { _containerTopLeft = Coordinates 0 0
      , _containerSize = Size 640 480
      , _containerName = "Base container"
      , _containerBackground = SolidColourBackground Colours.black
      , _containerOnClick = Nothing
      , _containerOnMouseDown = Nothing
      , _containerOnMouseUp = Nothing
      , _containerList =
          [ showComponent (sm ^. spaceStationModelQuitConfirmationShown) $ quitConfirmation t
          , infoPanel t sm sId gd
          , mainPanel t sm sId gd
          ]
      }
    , st
    )

uiView t sm sId gd = do
  -- since we don't have correct textures at hand, free the old ones and create new ones
  logDebug "freeing textures..."
  free t
  logDebug "generating space station textures..."
  textures <- asks appTextures
  renderer <- asks appRenderer
  config <- asks appConfig

  infoContainer <- createDialogTexture renderer textures $ Size 640 28
  mainContainer <- createDialogTexture renderer textures $ Size 640 452
  dockBtn <- createButtonTexture renderer textures (Just "dock") Nothing BlueButton (Size 50 18) 
  mapBtn <- createButtonTexture renderer textures (Just "map") Nothing BlueButton (Size 50 18) 
  marketBtn <- createButtonTexture renderer textures (Just "market") Nothing BlueButton (Size 50 18) 
  jobsBtn <- createButtonTexture renderer textures (Just "jobs") Nothing BlueButton (Size 50 18) 
  newsBtn <- createButtonTexture renderer textures (Just "news") Nothing BlueButton (Size 50 18)
  xBtn <- createButtonTexture renderer textures Nothing (Just SymbolX) RedButton (Size 18 18)

  let spaceStations = gd ^.. gameDataSystems . folded 
                              . starSystemSpaceStations . folded 
                              . filtered (\sp -> _spaceStationId sp == sId)
      planetaryStations = gd ^.. gameDataSystems . folded 
                              . starSystemPlanets . folded 
                              . planetSpaceStations . folded 
                              . filtered (\sp -> _spaceStationId sp == sId)
      station = safeHead $ spaceStations ++ planetaryStations
      stationName = fromMaybe "Unknown" ((_unSpaceStationName . _spaceStationName) <$> station)

  infoBar <- createText renderer textures GreyText Enabled (Size 250 18) $ "docked in " <> stationName

  quitDialog <- createDialogTexture renderer textures $ Size 141 54
  okBtn <- createButtonTexture renderer textures (Just "ok") Nothing BlueButton (Size 50 18)
  cancelBtn <- createButtonTexture renderer textures (Just "cancel") Nothing BlueButton (Size 50 18)
  confirmationText <- createText renderer textures GreyText Enabled (Size 110 16) "are you sure?"

  starMapBg <- createInsetTexture renderer textures $ Size 630 442

  controlsBg <- createDialogTexture renderer textures $ Size 60 60
  leftBtn <- createButtonTexture renderer textures Nothing (Just SymbolLeftArrow) BlueButton (Size 16 16)
  rightBtn <- createButtonTexture renderer textures Nothing (Just SymbolRightArrow) BlueButton (Size 16 16)
  upBtn <- createButtonTexture renderer textures Nothing (Just SymbolUpArrow) BlueButton (Size 16 16)
  downBtn <- createButtonTexture renderer textures Nothing (Just SymbolDownArrow) BlueButton (Size 16 16)
  zoomBtn <- createButtonTexture renderer textures Nothing (Just SymbolPlus) BlueButton (Size 16 16)

  let
    (gw, gh) = config ^. appConfigGalaxySize

  stars <- createStarMap renderer 
                         (textures ^. texturesSmallStar) 
                         (Size gw gh) $ 
                         gd ^. gameDataSystems

  let 
    smTxt = StarmapTextures
      { _starmapTexturesInfoPanel  = iPanelTxt
      , _starmapTexturesControls   = mcTxt
      , _starMapTexturesBackground = starMapBg
      , _starMapTexturesStars      = stars
      }
    iPanelTxt = MapInfoPanelTextures
      { 

      }
    mcTxt = MapControlsTextures
      { _mapControlsTexturesBackground = controlsBg
      , _mapControlsTexturesUp         = upBtn
      , _mapControlsTexturesDown       = downBtn
      , _mapControlsTexturesLeft       = leftBtn
      , _mapControlsTexturesRight      = rightBtn
      , _mapControlsTexturesZoom       = zoomBtn
      }
    txt = SpaceStationTextures
      { _spaceStationTexturesMainContainer    = mainContainer
      , _spaceStationTexturesInfoContainer    = infoContainer
      , _spaceStationTexturesQuitDialog       = quitDialog
      , _spaceStationTexturesDockButton       = dockBtn
      , _spaceStationTexturesMapButton        = mapBtn
      , _spaceStationTexturesMarketkButton    = marketBtn
      , _spaceStationTexturesJobsButton       = jobsBtn
      , _spaceStationTexturesNewsButton       = newsBtn
      , _spaceStationTexturesXButton          = xBtn
      , _spaceStationTexturesInfoBar          = infoBar
      , _spaceStationTexturesOkButton         = okBtn
      , _spaceStationTexturesCancelButton     = cancelBtn
      , _spaceStationTexturesConfirmationText = confirmationText
      , _spaceStationTexturesStarMap          = smTxt
      , _spaceStationTexturesControls         = mcTxt
      }

  uiView (SpaceStationUITextures txt) sm sId gd


-- |Build a handy accessor that can be used to get and set a specific field inside SpaceStationModel
accessor :: Lens' SpaceStationModel a -> VMSelector a
accessor f =
  ( \m ->
      m ^? _DockedInSpaceStation . _2 . f
  , \m vm ->
      m & _DockedInSpaceStation . _2 . f .~ vm
  )


-- |Use given message to update the model
update :: RandomGen g => Model -> SpaceStationMessage -> Rand g Model
update m msg = 
  case msg of
    SSQuitClicked ->
      return $ m & _DockedInSpaceStation . _2 . spaceStationModelQuitConfirmationShown .~ True

    SSQuitCancelled ->
      return $ m & _DockedInSpaceStation . _2 . spaceStationModelQuitConfirmationShown .~ False

    SSQuitConfirmed ->
      mainMenu (m ^. tick)

    SSDockClicked ->
      return $ m & _DockedInSpaceStation . _2 . spaceStationModelCurrentView .~ DockView

    SSMapClicked ->
      return $ m & _DockedInSpaceStation . _2 . spaceStationModelCurrentView .~ MapView

    SSMarketClicked ->
      return $ m & _DockedInSpaceStation . _2 . spaceStationModelCurrentView .~ MarketView

    SSJobsClicked ->
      return $ m & _DockedInSpaceStation . _2 . spaceStationModelCurrentView .~ JobsView

    SSNewsClicked ->
      return $ m & _DockedInSpaceStation . _2 . spaceStationModelCurrentView .~ NewsView


-- |'Components.Base.UIComponent' containing dialog defition with Ok and Cancel buttons,
-- asking the user if they're sure that they want to quit back to the main menu.
quitConfirmation :: SpaceStationTextures -> UIComponent
quitConfirmation t =
  modalDialog Nothing $ Container
    { _containerTopLeft = Coordinates 250 213
    , _containerSize = Size 141 54
    , _containerName = "Confirmation dialog"
    , _containerBackground = BackgroundTexture $ t ^. spaceStationTexturesQuitDialog
    , _containerOnClick = Nothing
    , _containerOnMouseDown = Nothing
    , _containerOnMouseUp = Nothing
    , _containerList =
        [ pack $ Picture
            { _pictureTopLeft     = Coordinates 35 15
            , _pictureSize        = Size 110 16
            , _pictureName        = "Confirmation text"
            , _pictureTexture     = t ^. spaceStationTexturesConfirmationText
            , _pictureOnMouseDown = Nothing
            , _pictureOnMouseUp   = Nothing
            , _pictureOnClick     = Nothing
            }
        , pack $ Button
            { _buttonTopLeft     = Coordinates 26 31
            , _buttonSize        = Size 52 18
            , _buttonName        = "Ok button"
            , _buttonTexture     = t ^. spaceStationTexturesOkButton
            , _buttonOnMouseDown = Nothing
            , _buttonOnMouseUp   = Nothing
            , _buttonOnClick     = Just $ SpaceStationMessage SSQuitConfirmed
            , _buttonVM          = accessor spaceStationModelOkButton
            }
        , pack $ Button
            { _buttonTopLeft     = Coordinates 82 31
            , _buttonSize        = Size 52 18
            , _buttonName        = "Cancel button"
            , _buttonTexture     = t ^. spaceStationTexturesCancelButton
            , _buttonOnMouseDown = Nothing
            , _buttonOnMouseUp   = Nothing
            , _buttonOnClick     = Just $ SpaceStationMessage SSQuitCancelled
            , _buttonVM          = accessor spaceStationModelCancelButton
            }
        ]
    }


-- |'Components.Base.UIComponent' containing a definition for the info panel shown at the top of
-- the screen while player is docked in a space station.
infoPanel :: SpaceStationTextures -> SpaceStationModel -> SpaceStationId -> GameData -> UIComponent
infoPanel t _ _ _ =
  pack $ Container
      { _containerTopLeft = Coordinates 0 0
      , _containerSize = Size 640 28
      , _containerName = "Base container"
      , _containerBackground = BackgroundTexture $ t ^. spaceStationTexturesInfoContainer
      , _containerOnClick = Nothing
      , _containerOnMouseDown = Nothing
      , _containerOnMouseUp = Nothing
      , _containerList =
          [ pack $ Button
              { _buttonTopLeft     = Coordinates 5 5
              , _buttonSize        = Size 52 18
              , _buttonName        = "Dock button"
              , _buttonTexture     = t ^. spaceStationTexturesDockButton
              , _buttonOnMouseDown = Nothing
              , _buttonOnMouseUp   = Nothing
              , _buttonOnClick     = Just $ SpaceStationMessage SSDockClicked
              , _buttonVM          = accessor spaceStationModelDockButton
              }
          , pack $ Button
              { _buttonTopLeft     = Coordinates 59 5
              , _buttonSize        = Size 52 18
              , _buttonName        = "Map button"
              , _buttonTexture     = t ^. spaceStationTexturesMapButton
              , _buttonOnMouseDown = Nothing
              , _buttonOnMouseUp   = Nothing
              , _buttonOnClick     = Just $ SpaceStationMessage SSMapClicked
              , _buttonVM          = accessor spaceStationModelMapButton
              }
          , pack $ Button
              { _buttonTopLeft     = Coordinates 112 5
              , _buttonSize        = Size 52 18
              , _buttonName        = "Market button"
              , _buttonTexture     = t ^. spaceStationTexturesMarketkButton
              , _buttonOnMouseDown = Nothing
              , _buttonOnMouseUp   = Nothing
              , _buttonOnClick     = Just $ SpaceStationMessage SSMarketClicked
              , _buttonVM          = accessor spaceStationModelMarketButton
              }
          , pack $ Button
              { _buttonTopLeft     = Coordinates 165 5
              , _buttonSize        = Size 52 18
              , _buttonName        = "Jobs button"
              , _buttonTexture     = t ^. spaceStationTexturesJobsButton
              , _buttonOnMouseDown = Nothing
              , _buttonOnMouseUp   = Nothing
              , _buttonOnClick     = Just $ SpaceStationMessage SSJobsClicked
              , _buttonVM          = accessor spaceStationModelJobsButton
              }
          , pack $ Button
              { _buttonTopLeft     = Coordinates 218 5
              , _buttonSize        = Size 52 18
              , _buttonName        = "News button"
              , _buttonTexture     = t ^. spaceStationTexturesNewsButton
              , _buttonOnMouseDown = Nothing
              , _buttonOnMouseUp   = Nothing
              , _buttonOnClick     = Just $ SpaceStationMessage SSNewsClicked
              , _buttonVM          = accessor spaceStationModelNewsButton
              }
          , pack $ Picture
              { _pictureTopLeft     = Coordinates 271 10
              , _pictureSize        = Size 250 18
              , _pictureName        = "Info bar text"
              , _pictureTexture     = t ^. spaceStationTexturesInfoBar
              , _pictureOnMouseDown = Nothing
              , _pictureOnMouseUp   = Nothing
              , _pictureOnClick     = Nothing
              }
          , pack $ Button
              { _buttonTopLeft     = Coordinates 617 5
              , _buttonSize        = Size 18 18
              , _buttonName        = "close button"
              , _buttonTexture     = t ^. spaceStationTexturesXButton
              , _buttonOnMouseDown = Nothing
              , _buttonOnMouseUp   = Nothing
              , _buttonOnClick     = Just $ SpaceStationMessage SSQuitClicked
              , _buttonVM          = accessor spaceStationModelXButton
              }
          ]
      }


-- |'Components.Base.UIComponent' containing a definition for the main panel shown while the
-- player is docked on to a space station.
mainPanel :: SpaceStationTextures -> SpaceStationModel -> SpaceStationId -> GameData -> UIComponent
mainPanel t m sId gd =
  pack $ Container
    { _containerTopLeft = Coordinates 0 28
    , _containerSize = Size 640 452
    , _containerName = "Main panel container"
    , _containerBackground = BackgroundTexture $ t ^. spaceStationTexturesMainContainer
    , _containerOnClick = Nothing
    , _containerOnMouseDown = Nothing
    , _containerOnMouseUp = Nothing
    , _containerList =
        case m ^. spaceStationModelCurrentView of
          DockView ->
            dockView t m sId gd
  
          MapView ->
            mapView t m sId gd

          MarketView ->
            marketView t m sId gd

          JobsView ->
            jobsView t m sId gd

          NewsView ->
            newsView t m sId gd
    }


dockView :: SpaceStationTextures -> SpaceStationModel -> SpaceStationId -> GameData -> [ UIComponent ]
dockView _ _ _ _ =
  [ pack $ NoComponent ]


mapView :: SpaceStationTextures -> SpaceStationModel -> SpaceStationId -> GameData -> [ UIComponent ]
mapView t _ _ _ =
  [ starMap $
      StarMapConfig
        { _starMapConfigTopLeft       = Coordinates 5 5
        , _starMapConfigSize          = Size 630 442
        , _starMapConfigBackground    = t ^. spaceStationTexturesMainContainer .to BackgroundTexture
        , _starMapConfigName          = "Star map"
        , _starMapConfigVM            = accessor spaceStationModelStarMap
        , _starMapConfigUpButton      = Nothing
        , _starMapConfigDownButton    = Nothing
        , _starMapConfigLeftButton    = Nothing
        , _starMapConfigRightButton   = Nothing
        , _starMapConfigStarSelected  = Nothing
        , _starMapConfigZoomChanged   = Nothing
        , _starMapConfigTextures      = t ^. spaceStationTexturesStarMap
        , _starMapConfigUpButtonVM    = accessor (spaceStationModelMapControls . mapControlsVMUpButton)
        , _starMapConfigDownButtonVM  = accessor (spaceStationModelMapControls . mapControlsVMDownButton)
        , _starMapConfigLeftButtonVM  = accessor (spaceStationModelMapControls . mapControlsVMLeftButton)
        , _starMapConfigRightButtonVM = accessor (spaceStationModelMapControls . mapControlsVMRightButton)
        , _starMapConfigZoomButtonVM  = accessor (spaceStationModelMapControls . mapControlsVMZoomButton)
        }
  ]


marketView :: SpaceStationTextures -> SpaceStationModel -> SpaceStationId -> GameData -> [ UIComponent ]
marketView _ _ _ _ =
  [ pack $ NoComponent ]


jobsView :: SpaceStationTextures -> SpaceStationModel -> SpaceStationId -> GameData -> [ UIComponent ]
jobsView _ _ _ _ =
  [ pack $ NoComponent ]


newsView :: SpaceStationTextures -> SpaceStationModel -> SpaceStationId -> GameData -> [ UIComponent ]
newsView _ _ _ _ =
  [ pack $ NoComponent ]
