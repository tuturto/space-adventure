-- | Silly utility module, used to demonstrate how to write a test
-- case.
module Util
  ( plus2
  , clamp
  ) where

import RIO

plus2 :: Int -> Int
plus2 = (+ 2)


-- |Clamp value between lower and upper bound
clamp :: Ord a => a -> (a, a) -> a
clamp value (lowerLimit, upperLimit)
   | value < lowerLimit = lowerLimit
   | value > upperLimit = upperLimit
   | otherwise          = value
