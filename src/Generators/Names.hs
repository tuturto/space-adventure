{-|
Module      : Generators.Names
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module Generators.Names
  ( module Generators.Names.Markov
  , module Generators.Names.SpaceStations
  )
  where

import Generators.Names.Markov
import Generators.Names.SpaceStations
