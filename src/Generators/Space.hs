{-|
Module      : Generators.Space
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module Generators.Space
  ( generateSpace
  )
  where

import Import
import RIO.List
import Control.Monad.Random.Strict
import Control.Lens ( _1, _2 )

import Generators.Names
import Model


generateSpace :: RandomGen g => AppConfig -> Rand g [StarSystem]
generateSpace c = do
  -- generate bunch of coordinates with a nice scatter
  xs <- getRandomRs (0, c ^. appConfigGalaxySize . _1)
  ys <- getRandomRs (0, c ^. appConfigGalaxySize . _2)

  let coords = take (c ^. appConfigStarLimit) $ map (\(hx, hy) -> HyperCoordinates hx hy) $ nub $ zip xs ys
      pairs = zip [0..] coords
  -- generate a star system on each of those coordinates
  mapM (generateStarSystem c) pairs


generateStarSystem :: RandomGen g => AppConfig -> (Int, HyperCoordinates) -> Rand g StarSystem
generateStarSystem c (i, coords) = do
  let
    starLimit = c ^. appConfigStarLimit

  s <- starSystemNameM
  starCount <- chooseM starCountDistribution
  stars <- mapM generateStar $
                take (fromMaybe 1 starCount) $ map ( (*) starLimit) [i..]

  planetCount <- getRandomR (0, 10)  
  hotZone <- getRandomR (2, 5)
  ecoZone <- getRandomR (0, 2)
  coldZone <- getRandomR (5, 7)

  let
    planetIds = take planetCount $ map ( (*) starLimit) [i..]
    zones = map (\x -> (x, HotZone)) (take hotZone planetIds) ++
            map (\x -> (x, EcoZone)) (take ecoZone (drop hotZone planetIds)) ++
            map (\x -> (x, ColdZone)) (take coldZone (drop (hotZone + ecoZone) planetIds)) ++
            map (\x -> (x, CometaryHalo)) (drop (hotZone + ecoZone + coldZone) planetIds)

  planets <- mapM generatePlanet zones

  stationCount <- getRandomR (0, 3)
  let
    stationIds = take stationCount $ map ( (*) starLimit ) [i..]

  stations <- mapM (generateSpaceStation s) stationIds

  return $ StarSystem
    { _starSystemId            = MkStarSystemId i
    , _starSystemName          = s
    , _starSystemLocation      = coords
    , _starSystemStars         = stars
    , _starSystemPlanets       = planets
    , _starSystemSpaceStations = stations
    }


generateStar :: RandomGen g => Int -> Rand g Star
generateStar i = do
  s <- starNameM  
  luminosity <- chooseM luminosityClassDistribution
  spectral <- chooseM (spectralClassDistribution $ fromMaybe LuminosityV luminosity)

  return $ Star
    { _starId              = MkStarId i
    , _starName            = s
    , _starSpectralClass   = fromMaybe SpectralM spectral
    , _starLuminosityClass = fromMaybe LuminosityV luminosity
    }


generatePlanet :: RandomGen g => (Int, PlanetZoneType) -> Rand g Planet
generatePlanet (i, z) = do
  s <- planetNameM
  pType <- chooseM (planetTypeDistribution z)
  let pType' = fromMaybe (case z of
                            HotZone ->
                              Asteroid
                              
                            EcoZone ->
                              Asteroid
                              
                            ColdZone ->
                              Asteroid
                              
                            CometaryHalo ->
                              Rogue) pType

  atmos <- chooseM (planetAtmostphereDistribution pType')
  let
    atmosphere = fromMaybe VacuumAtmosphere atmos

  life <- chooseM (lifeformsDistribution pType' atmosphere)  
  hLevel <- chooseM (planetHabitationDistribution pType')
  gType <- chooseM planetGovermentTypeDistribution
  let
    lifeforms = fromMaybe Nothing life
    habitationLevel = fromMaybe NoHabitation hLevel
    govermentType = fromMaybe DirectDemocracy gType

  tLevel <- chooseM (planetTechLevelDistribution pType' habitationLevel)
  let
    techLevel = fromMaybe CommonTech tLevel

  return $ Planet
    { _planetId               = MkPlanetId i
    , _planetName             = s
    , _planetType             = pType'
    , _planetAtmostphere      = atmosphere
    , _planetHighestLifeForms = lifeforms
    , _planetHabitation       = habitationLevel
    , _planetGovermentType    = govermentType
    , _planetTechLevel        = techLevel
    , _planetSpaceStations    = []
    , _planetMoons            = []
    }


generateSpaceStation :: RandomGen g => StarSystemName -> Int -> Rand g SpaceStation
generateSpaceStation sn i = do
  t <- chooseM spaceStationTypeDistribution
  tl <- chooseM spaceStationTechLevelDistribution
  let
    sType = fromMaybe ResearchStation t
    techLevel = fromMaybe HighTech tl
  s <- spaceStationNameM sn sType
  let
    station = SpaceStation
      { _spaceStationId        = MkSpaceStationId i
      , _spaceStationName      = s
      , _spaceStationTechLevel = techLevel
      , _spaceStationMarket    = Market [] [] -- TODO: create market
      , _spaceStationType      = sType
      }
  return station


-- | Likelyhood of a star system having a specified amount of stars
starCountDistribution :: [Frequency Int]
starCountDistribution =
  [ Frequency 50 1
  , Frequency 25 2
  , Frequency 5  3
  , Frequency 1  4
  ]


-- | Distribution of planet types on each zone
planetTypeDistribution :: PlanetZoneType -> [Frequency PlanetType]
planetTypeDistribution HotZone =
  [ Frequency 50 Geomorteus
  , Frequency 20 Asteroid
  , Frequency 30 Desert
  , Frequency 2 Variable
  , Frequency 1 Demon
  ]

planetTypeDistribution EcoZone =
  [ Frequency 20 Geothermal
  , Frequency 20 Geoinactive
  , Frequency 5 Asteroid
  , Frequency 15 Geoplastic
  , Frequency 15 Geometallic
  , Frequency 15 Geocrystalline
  , Frequency 5 Desert
  , Frequency 3 Adaptable
  , Frequency 3 Marginal
  , Frequency 2 Terrestrial
  , Frequency 10 Reducing
  , Frequency 2 Pelagic
  , Frequency 2 Variable
  , Frequency 1 Demon
  ]

planetTypeDistribution ColdZone =
  [ Frequency 10 Geothermal
  , Frequency 10 Geoinactive
  , Frequency 5 Asteroid
  , Frequency 5 Desert
  , Frequency 20 GasSuperGiant
  , Frequency 40 GasGiant
  , Frequency 2 Glaciated
  , Frequency 2 Variable
  , Frequency 10 Ultragiant
  , Frequency 1 Demon
  ]

planetTypeDistribution CometaryHalo =
  [ Frequency 1 Rogue ]


luminosityClassDistribution :: [Frequency LuminosityClass]
luminosityClassDistribution =
  [ Frequency 2 Luminosity0      -- hyper giant
  , Frequency 5 LuminosityI      -- super giant
  , Frequency 15 LuminosityII    -- bright giant
  , Frequency 20 LuminosityIII   -- giant
  , Frequency 25 LuminosityIV    -- subgiant
  , Frequency 50 LuminosityV     -- main-sequence
  , Frequency 40 LuminosityVI    -- subdwarf
  , Frequency 30 LuminosityVII   -- white dwarf
  ]


spectralClassDistribution :: LuminosityClass -> [Frequency SpectralClass]
spectralClassDistribution Luminosity0 = -- hyper giant
  [ Frequency 50 SpectralO
  , Frequency 250 SpectralB
  , Frequency 900 SpectralA
  , Frequency 3030 SpectralF
  , Frequency 6000 SpectralG
  , Frequency 8000 SpectralK
  , Frequency 40000 SpectralM
  ]

spectralClassDistribution LuminosityI = -- super giant
  [ Frequency 50 SpectralO
  , Frequency 250 SpectralB
  , Frequency 900 SpectralA
  , Frequency 3030 SpectralF
  , Frequency 6000 SpectralG
  , Frequency 8000 SpectralK
  , Frequency 40000 SpectralM
  ]

spectralClassDistribution LuminosityII = -- bright giant
  [ Frequency 25 SpectralO
  , Frequency 200 SpectralB
  , Frequency 600 SpectralA
  , Frequency 3030 SpectralF
  , Frequency 6000 SpectralG
  , Frequency 10000 SpectralK
  , Frequency 50000 SpectralM
  ]

spectralClassDistribution LuminosityIII = -- giant
  [ Frequency 10 SpectralO
  , Frequency 200 SpectralB
  , Frequency 600 SpectralA
  , Frequency 3030 SpectralF
  , Frequency 7000 SpectralG
  , Frequency 11000 SpectralK
  , Frequency 60000 SpectralM
  ]

spectralClassDistribution LuminosityIV = -- subgiant
  [ Frequency 1 SpectralO
  , Frequency 125 SpectralB
  , Frequency 625 SpectralA
  , Frequency 3030 SpectralF
  , Frequency 7200 SpectralG
  , Frequency 11000 SpectralK
  , Frequency 70000 SpectralM
  ]

spectralClassDistribution LuminosityV = -- main-sequence
  [ Frequency 1 SpectralO
  , Frequency 125 SpectralB
  , Frequency 625 SpectralA
  , Frequency 3030 SpectralF
  , Frequency 7500 SpectralG
  , Frequency 12000 SpectralK
  , Frequency 76000 SpectralM
  ]

spectralClassDistribution LuminosityVI = -- subdwarf
  [ Frequency 50 SpectralB
  , Frequency 150 SpectralA
  , Frequency 1500 SpectralF
  , Frequency 5000 SpectralG
  , Frequency 12000 SpectralK
  , Frequency 76000 SpectralM
  ]

spectralClassDistribution LuminosityVII = -- white dwarf
  [ Frequency 10 SpectralB
  , Frequency 100 SpectralA
  , Frequency 1500 SpectralF
  , Frequency 5000 SpectralG
  , Frequency 15000 SpectralK
  , Frequency 90000 SpectralM
  , Frequency 100000 SpectralL
  , Frequency 120000 SpectralT
  ]


spaceStationTypeDistribution :: [Frequency SpaceStationType]
spaceStationTypeDistribution =
  [ Frequency 10 ResearchStation
  , Frequency 10 TradingPost
  , Frequency 10 MilitaryBase
  ]


spaceStationTechLevelDistribution :: [Frequency TechLevel]
spaceStationTechLevelDistribution =
  [ Frequency 30 HighTech
  , Frequency 10 AdvancedTech
  , Frequency 5 HyperAdvancedTech
  ]


planetHabitationDistribution :: PlanetType -> [Frequency HabitationLevel]
planetHabitationDistribution Geothermal = 
  [ Frequency 1 NoHabitation ]

planetHabitationDistribution Geomorteus = 
  [ Frequency 1 NoHabitation ]

planetHabitationDistribution Geoinactive = 
  [ Frequency 99 NoHabitation
  , Frequency 1 SmallColonies
  ]

planetHabitationDistribution Asteroid = 
  [ Frequency 50 NoHabitation
  , Frequency 2 SmallColonies
  ]

planetHabitationDistribution Geoplastic = 
  [ Frequency 1 NoHabitation ]

planetHabitationDistribution Geometallic = 
  [ Frequency 50 NoHabitation
  , Frequency 10 SmallColonies
  , Frequency 1 BigColonies
  ]

planetHabitationDistribution Geocrystalline = 
  [ Frequency 40 NoHabitation
  , Frequency 10 SmallColonies
  , Frequency 5 BigColonies
  ]

planetHabitationDistribution Desert =
  [ Frequency 40 NoHabitation
  , Frequency 10 SmallColonies
  , Frequency 5 BigColonies
  ]

planetHabitationDistribution GasSuperGiant =
  [ Frequency 1 NoHabitation ]

planetHabitationDistribution GasGiant = 
  [ Frequency 1 NoHabitation ]

planetHabitationDistribution Adaptable =
  [ Frequency 20 NoHabitation
  , Frequency 10 SmallColonies
  , Frequency 5 BigColonies
  ]

planetHabitationDistribution Marginal =
  [ Frequency 20 NoHabitation
  , Frequency 20 SmallColonies
  , Frequency 5 BigColonies
  ]

planetHabitationDistribution Terrestrial =
  [ Frequency 5 SmallColonies
  , Frequency 10 BigColonies
  , Frequency 20 SparseHabitation
  , Frequency 40 MediumHabitation
  , Frequency 20 DenseHabitation
  ]

planetHabitationDistribution Reducing =
  [ Frequency 20 NoHabitation
  , Frequency 20 SmallColonies
  , Frequency 20 BigColonies
  ]

planetHabitationDistribution Pelagic =
  [ Frequency 5 NoHabitation
  , Frequency 5 SmallColonies
  , Frequency 20 BigColonies
  , Frequency 10 SparseHabitation
  ]

planetHabitationDistribution Glaciated =
  [ Frequency 5 NoHabitation
  , Frequency 5 SmallColonies
  , Frequency 20 BigColonies
  , Frequency 10 SparseHabitation
  ]

planetHabitationDistribution Variable =
  [ Frequency 10 NoHabitation
  , Frequency 20 SmallColonies
  , Frequency 10 BigColonies
  ]

planetHabitationDistribution Rogue =
  [ Frequency 20 NoHabitation
  , Frequency 1 SmallColonies
  ]

planetHabitationDistribution Ultragiant =
  [ Frequency 1 NoHabitation ]

planetHabitationDistribution Demon =
  [ Frequency 1 NoHabitation ]


planetGovermentTypeDistribution :: [ Frequency GovermentType ]
planetGovermentTypeDistribution =
  [ Frequency 10 DirectDemocracy
  , Frequency 10 RepublicGovernment
  , Frequency 10 ParliamentaryGovernment
  , Frequency 10 Authoritarian
  , Frequency 10 Totalitarian
  , Frequency 10 Monarchy
  , Frequency 10 Oligarchy
  , Frequency 10 Technocracy
  , Frequency 10 Theocracy
  , Frequency 10 Dictatorship
  , Frequency 10 Communist
  , Frequency 10 Aristocratic
  ]


planetTechLevelDistribution :: PlanetType -> HabitationLevel -> [ Frequency TechLevel ]
planetTechLevelDistribution _ NoHabitation =
  [ Frequency 1 Primitive ]

-- TODO: finish defining this
planetTechLevelDistribution pType hLevel
  | hLevel == NoHabitation = [ Frequency 1 Primitive ]
  | pType == Terrestrial 
    && hLevel <= SparseHabitation = [ Frequency 6 Primitive
                                    , Frequency 12 LowTech
                                    , Frequency 25 CommonTech
                                    , Frequency 50 HighTech
                                    , Frequency 25 AdvancedTech
                                    , Frequency 12 HyperAdvancedTech
                                    ]
  | pType == Terrestrial = [ Frequency 3 Primitive
                           , Frequency 6 LowTech
                           , Frequency 12 CommonTech
                           , Frequency 25 HighTech
                           , Frequency 50 AdvancedTech
                           , Frequency 25 HyperAdvancedTech
                           ]
  | pType == Pelagic
    && hLevel == SparseHabitation  = [ Frequency 6 LowTech
                                     , Frequency 12 CommonTech
                                     , Frequency 25 HighTech
                                     , Frequency 50 AdvancedTech
                                     , Frequency 25 HyperAdvancedTech
                                     ]
  | pType == Pelagic
    && hLevel == BigColonies  = [ Frequency 25 LowTech
                                , Frequency 50 CommonTech
                                , Frequency 25 HighTech
                                , Frequency 12 AdvancedTech
                                , Frequency 6 HyperAdvancedTech
                                ]
  | pType == Pelagic = [ Frequency 12 Primitive
                       , Frequency 25 LowTech
                       , Frequency 50 CommonTech
                       , Frequency 25 HighTech
                       , Frequency 12 AdvancedTech
                       ]
  | pType == Reducing
    && hLevel == BigColonies = [ Frequency 50 CommonTech
                               , Frequency 25 HighTech
                               , Frequency 12 AdvancedTech
                               , Frequency 6 HyperAdvancedTech
                               ]
  | pType == Reducing
    && hLevel == SmallColonies = [ Frequency 50 CommonTech
                                 , Frequency 25 HighTech
                                 , Frequency 12 AdvancedTech
                                 ]
  | pType == Glaciated
    && hLevel <= BigColonies = [ Frequency 25 LowTech
                               , Frequency 50 CommonTech
                               , Frequency 25 HighTech
                               , Frequency 12 AdvancedTech
                               ]
  | pType == Glaciated = [ Frequency 25 LowTech
                         , Frequency 50 CommonTech
                         , Frequency 25 HighTech
                         , Frequency 12 AdvancedTech
                         , Frequency 6 HyperAdvancedTech
                         ]
  | otherwise  = [ Frequency 50 CommonTech
                 , Frequency 25 HighTech
                 , Frequency 12 AdvancedTech
                 , Frequency 6 HyperAdvancedTech
                 ]


planetAtmostphereDistribution :: PlanetType -> [ Frequency Atmosphere ]
planetAtmostphereDistribution Geothermal = 
  [ Frequency 1 $ VacuumAtmosphere
  , Frequency 1 $ TraceAtmosphere CausticAtmosphere
  , Frequency 1 $ TraceAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ TraceAtmosphere PoisonousAtmosphere
  , Frequency 1 $ ThinAtmosphere CausticAtmosphere
  , Frequency 1 $ ThinAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ ThinAtmosphere PoisonousAtmosphere
  ]

planetAtmostphereDistribution Geomorteus =
  [ Frequency 1 $ VacuumAtmosphere
  , Frequency 1 $ TraceAtmosphere CausticAtmosphere  
  , Frequency 1 $ TraceAtmosphere RadiologicalAtmosphere  
  , Frequency 1 $ TraceAtmosphere PoisonousAtmosphere  
  ]

planetAtmostphereDistribution Geoinactive =
  [ Frequency 1 $ VacuumAtmosphere ]

planetAtmostphereDistribution Asteroid =
  [ Frequency 1 $ VacuumAtmosphere ]

planetAtmostphereDistribution Geoplastic =
  [ Frequency 1 $ VacuumAtmosphere
  , Frequency 1 $ TraceAtmosphere CausticAtmosphere
  , Frequency 1 $ TraceAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ TraceAtmosphere PoisonousAtmosphere
  , Frequency 1 $ ThinAtmosphere CausticAtmosphere
  , Frequency 1 $ ThinAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ ThinAtmosphere PoisonousAtmosphere
  ]

planetAtmostphereDistribution Geometallic =
  [ Frequency 1 $ VacuumAtmosphere
  , Frequency 1 $ TraceAtmosphere CausticAtmosphere
  , Frequency 1 $ TraceAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ TraceAtmosphere PoisonousAtmosphere
  , Frequency 1 $ ThinAtmosphere CausticAtmosphere
  , Frequency 1 $ ThinAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ ThinAtmosphere PoisonousAtmosphere
  , Frequency 1 $ StandardAtmosphere CausticAtmosphere
  , Frequency 1 $ StandardAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ StandardAtmosphere PoisonousAtmosphere
  ]

planetAtmostphereDistribution Geocrystalline =
  [ Frequency 1 $ VacuumAtmosphere
  , Frequency 1 $ TraceAtmosphere CausticAtmosphere
  , Frequency 1 $ TraceAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ TraceAtmosphere PoisonousAtmosphere
  , Frequency 1 $ ThinAtmosphere CausticAtmosphere
  , Frequency 1 $ ThinAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ ThinAtmosphere PoisonousAtmosphere
  , Frequency 1 $ StandardAtmosphere CausticAtmosphere
  , Frequency 1 $ StandardAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ StandardAtmosphere PoisonousAtmosphere
  , Frequency 1 $ HighAtmosphere CausticAtmosphere
  , Frequency 1 $ HighAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ HighAtmosphere PoisonousAtmosphere
  ]

planetAtmostphereDistribution Desert =
  [ Frequency 1 $ VacuumAtmosphere
  , Frequency 1 $ TraceAtmosphere CausticAtmosphere
  , Frequency 1 $ TraceAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ TraceAtmosphere PoisonousAtmosphere
  , Frequency 1 $ ThinAtmosphere CausticAtmosphere
  , Frequency 1 $ ThinAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ ThinAtmosphere PoisonousAtmosphere
  , Frequency 1 $ StandardAtmosphere CausticAtmosphere
  , Frequency 1 $ StandardAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ StandardAtmosphere PoisonousAtmosphere
  , Frequency 1 $ HighAtmosphere CausticAtmosphere
  , Frequency 1 $ HighAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ HighAtmosphere PoisonousAtmosphere
  , Frequency 1 $ VeryHighAtmosphere CausticAtmosphere
  , Frequency 1 $ VeryHighAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ VeryHighAtmosphere PoisonousAtmosphere
  ]

planetAtmostphereDistribution GasSuperGiant =
  [ Frequency 1 $ VeryHighAtmosphere CausticAtmosphere
  , Frequency 1 $ VeryHighAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ VeryHighAtmosphere PoisonousAtmosphere
  ]

planetAtmostphereDistribution GasGiant =
  [ Frequency 1 $ HighAtmosphere CausticAtmosphere
  , Frequency 1 $ HighAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ HighAtmosphere PoisonousAtmosphere
  , Frequency 1 $ VeryHighAtmosphere CausticAtmosphere
  , Frequency 1 $ VeryHighAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ VeryHighAtmosphere PoisonousAtmosphere
  ]

planetAtmostphereDistribution Adaptable =
  [ Frequency 1 $ TraceAtmosphere PoisonousAtmosphere
  , Frequency 1 $ ThinAtmosphere BreathableAtmosphere
  , Frequency 1 $ ThinAtmosphere PoisonousAtmosphere
  ]

planetAtmostphereDistribution Marginal = 
  [ Frequency 1 $ ThinAtmosphere BreathableAtmosphere
  , Frequency 1 $ ThinAtmosphere PoisonousAtmosphere
  , Frequency 1 $ StandardAtmosphere BreathableAtmosphere
  , Frequency 1 $ StandardAtmosphere PoisonousAtmosphere
  , Frequency 1 $ HighAtmosphere BreathableAtmosphere
  , Frequency 1 $ HighAtmosphere PoisonousAtmosphere
  ]

planetAtmostphereDistribution Terrestrial =
  [ Frequency 1 $ ThinAtmosphere BreathableAtmosphere
  , Frequency 1 $ StandardAtmosphere BreathableAtmosphere
  , Frequency 1 $ HighAtmosphere BreathableAtmosphere
  ]

planetAtmostphereDistribution Reducing =
  [ Frequency 1 $ HighAtmosphere BreathableAtmosphere
  , Frequency 1 $ HighAtmosphere CausticAtmosphere
  , Frequency 1 $ HighAtmosphere PoisonousAtmosphere
  , Frequency 1 $ VeryHighAtmosphere CausticAtmosphere
  , Frequency 1 $ VeryHighAtmosphere PoisonousAtmosphere
  ]

planetAtmostphereDistribution Pelagic =
  [ Frequency 1 $ ThinAtmosphere BreathableAtmosphere
  , Frequency 1 $ ThinAtmosphere PoisonousAtmosphere
  , Frequency 1 $ StandardAtmosphere BreathableAtmosphere
  , Frequency 1 $ StandardAtmosphere PoisonousAtmosphere
  , Frequency 1 $ HighAtmosphere BreathableAtmosphere
  , Frequency 1 $ HighAtmosphere PoisonousAtmosphere
  ]

planetAtmostphereDistribution Glaciated =
  [ Frequency 1 $ ThinAtmosphere BreathableAtmosphere
  , Frequency 1 $ ThinAtmosphere PoisonousAtmosphere
  , Frequency 1 $ StandardAtmosphere BreathableAtmosphere
  , Frequency 1 $ StandardAtmosphere PoisonousAtmosphere
  , Frequency 1 $ HighAtmosphere BreathableAtmosphere
  , Frequency 1 $ HighAtmosphere PoisonousAtmosphere
  ]

planetAtmostphereDistribution Variable =
  [ Frequency 1 $ VacuumAtmosphere
  , Frequency 1 $ TraceAtmosphere CausticAtmosphere
  , Frequency 1 $ TraceAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ TraceAtmosphere PoisonousAtmosphere
  , Frequency 1 $ ThinAtmosphere BreathableAtmosphere
  , Frequency 1 $ ThinAtmosphere CausticAtmosphere
  , Frequency 1 $ ThinAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ ThinAtmosphere PoisonousAtmosphere
  , Frequency 1 $ StandardAtmosphere BreathableAtmosphere
  , Frequency 1 $ StandardAtmosphere CausticAtmosphere
  , Frequency 1 $ StandardAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ StandardAtmosphere PoisonousAtmosphere
  , Frequency 1 $ HighAtmosphere BreathableAtmosphere
  , Frequency 1 $ HighAtmosphere CausticAtmosphere
  , Frequency 1 $ HighAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ HighAtmosphere PoisonousAtmosphere
  , Frequency 1 $ VeryHighAtmosphere CausticAtmosphere
  , Frequency 1 $ VeryHighAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ VeryHighAtmosphere PoisonousAtmosphere
  ]

planetAtmostphereDistribution Rogue =
  [ Frequency 1 $ VacuumAtmosphere
  , Frequency 1 $ TraceAtmosphere CausticAtmosphere
  , Frequency 1 $ TraceAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ TraceAtmosphere PoisonousAtmosphere
  , Frequency 1 $ ThinAtmosphere BreathableAtmosphere
  , Frequency 1 $ ThinAtmosphere CausticAtmosphere
  , Frequency 1 $ ThinAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ ThinAtmosphere PoisonousAtmosphere
  ]

planetAtmostphereDistribution Ultragiant =
  [ Frequency 1 $ VacuumAtmosphere ]

planetAtmostphereDistribution Demon =
  [ Frequency 1 $ ThinAtmosphere CausticAtmosphere
  , Frequency 1 $ ThinAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ ThinAtmosphere PoisonousAtmosphere
  , Frequency 1 $ StandardAtmosphere CausticAtmosphere
  , Frequency 1 $ StandardAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ StandardAtmosphere PoisonousAtmosphere
  , Frequency 1 $ HighAtmosphere CausticAtmosphere
  , Frequency 1 $ HighAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ HighAtmosphere PoisonousAtmosphere
  , Frequency 1 $ VeryHighAtmosphere CausticAtmosphere
  , Frequency 1 $ VeryHighAtmosphere RadiologicalAtmosphere
  , Frequency 1 $ VeryHighAtmosphere PoisonousAtmosphere
  ]

lifeformsDistribution :: PlanetType -> Atmosphere -> [ Frequency (Maybe Lifeforms) ]
lifeformsDistribution _ VacuumAtmosphere =
  [ Frequency 1 Nothing ]

lifeformsDistribution Geoplastic _ =
  [ Frequency 1 Nothing
  , Frequency 1 (Just Bacteria)
  ]

lifeformsDistribution Geoinactive _ =
  [ Frequency 1 Nothing
  , Frequency 1 (Just Bacteria)
  ]

lifeformsDistribution Geocrystalline _ =
  [ Frequency 1 Nothing
  , Frequency 1 (Just Bacteria)
  , Frequency 1 (Just Protozoa)
  ]

lifeformsDistribution Desert atm =
  case atm of 
    TraceAtmosphere _ ->
      [ Frequency 1 Nothing
      , Frequency 1 (Just Bacteria)
      , Frequency 1 (Just Protozoa)
      ]

    _ -> 
      [ Frequency 1 Nothing
      , Frequency 1 (Just Bacteria)
      , Frequency 1 (Just Protozoa)
      , Frequency 1 (Just Fungi)
      ]

lifeformsDistribution GasGiant _ =
  [ Frequency 1 Nothing
  , Frequency 1 (Just Bacteria)
  ]

lifeformsDistribution Adaptable _ =
  [ Frequency 1 Nothing
  , Frequency 1 (Just Bacteria)
  , Frequency 1 (Just Protozoa)
  ]

lifeformsDistribution Marginal atm =
  case atm of
    ThinAtmosphere BreathableAtmosphere -> 
      [ Frequency 1 Nothing
      , Frequency 1 (Just Bacteria)
      , Frequency 1 (Just Protozoa)
      , Frequency 1 (Just Fungi)
      , Frequency 1 (Just Fish)
      ]

    ThinAtmosphere PoisonousAtmosphere -> 
      [ Frequency 1 Nothing
      , Frequency 1 (Just Bacteria)
      , Frequency 1 (Just Protozoa)
      , Frequency 1 (Just Fungi)
      ]

    StandardAtmosphere BreathableAtmosphere -> 
      [ Frequency 1 Nothing
      , Frequency 1 (Just Bacteria)
      , Frequency 1 (Just Protozoa)
      , Frequency 1 (Just Fungi)
      , Frequency 1 (Just Fish)
      ]

    StandardAtmosphere PoisonousAtmosphere -> 
      [ Frequency 1 Nothing
      , Frequency 1 (Just Bacteria)
      , Frequency 1 (Just Protozoa)
      , Frequency 1 (Just Fungi)
      ]

    HighAtmosphere BreathableAtmosphere -> 
      [ Frequency 1 Nothing
      , Frequency 1 (Just Bacteria)
      , Frequency 1 (Just Protozoa)
      , Frequency 1 (Just Fungi)
      , Frequency 1 (Just Fish)
      ]

    HighAtmosphere PoisonousAtmosphere -> 
      [ Frequency 1 Nothing
      , Frequency 1 (Just Bacteria)
      , Frequency 1 (Just Protozoa)
      , Frequency 1 (Just Fungi)
      ]

    _ -> 
      [ Frequency 1 Nothing
      , Frequency 1 (Just Bacteria)
      , Frequency 1 (Just Protozoa)
      , Frequency 1 (Just Fungi)
      ]

lifeformsDistribution Variable _ =
  [ Frequency 1 Nothing
  , Frequency 1 (Just Bacteria)
  , Frequency 1 (Just Protozoa)
  ]

lifeformsDistribution Demon _ =
  [ Frequency 1 Nothing
  , Frequency 1 (Just Bacteria)
  , Frequency 1 (Just Protozoa)
  , Frequency 1 (Just Fungi)
  ]

lifeformsDistribution Rogue atm =
  case atm of
    TraceAtmosphere _ ->
      [ Frequency 1 Nothing
      , Frequency 1 (Just Bacteria)
      , Frequency 1 (Just Protozoa)
      ]

    ThinAtmosphere BreathableAtmosphere ->
      [ Frequency 1 Nothing
      , Frequency 1 (Just Bacteria)
      , Frequency 1 (Just Protozoa)
      , Frequency 1 (Just Fungi)
      , Frequency 1 (Just Fish)
      , Frequency 1 (Just Amphibians)
      ]

    _ ->
      [ Frequency 1 Nothing
      , Frequency 1 (Just Bacteria)
      , Frequency 1 (Just Protozoa)
      ]

lifeformsDistribution Terrestrial _ =
  [ Frequency 1 (Just Amphibians)
  , Frequency 1 (Just Reptiles)
  , Frequency 1 (Just Birds)
  , Frequency 1 (Just Mammals)
  ]

lifeformsDistribution Reducing atm =
  case atm of
    HighAtmosphere BreathableAtmosphere ->
      [ Frequency 1 (Just Bacteria)
      , Frequency 1 (Just Protozoa)
      , Frequency 1 (Just Fungi)
      , Frequency 1 (Just Fish)
      ]

    _ ->
      [ Frequency 1 Nothing
      , Frequency 1 (Just Bacteria)
      , Frequency 1 (Just Protozoa)
      , Frequency 1 (Just Fungi)
      ]

lifeformsDistribution Pelagic _ =
  [ Frequency 1 (Just Bacteria)
  , Frequency 1 (Just Protozoa)
  , Frequency 1 (Just Fungi)
  , Frequency 1 (Just Fish)
  ]

lifeformsDistribution Glaciated atm =
  case atm of
    ThinAtmosphere BreathableAtmosphere ->
      [ Frequency 1 (Just Bacteria)
      , Frequency 1 (Just Protozoa)
      , Frequency 1 (Just Fungi)
      ]
    
    StandardAtmosphere BreathableAtmosphere ->
      [ Frequency 1 (Just Bacteria)
      , Frequency 1 (Just Protozoa)
      , Frequency 1 (Just Fungi)
      , Frequency 1 (Just Birds)
      , Frequency 1 (Just Mammals)
      ]

    HighAtmosphere BreathableAtmosphere ->
      [ Frequency 1 (Just Bacteria)
      , Frequency 1 (Just Protozoa)
      , Frequency 1 (Just Fungi)
      ]

    _ ->
      [ Frequency 1 (Just Bacteria)
      , Frequency 1 (Just Protozoa)
      , Frequency 1 (Just Fungi)
      ]

lifeformsDistribution _ _ =
  [ Frequency 1 Nothing ]
