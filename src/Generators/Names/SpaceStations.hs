{-|
Module      : Generators.Names.SpaceStations
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module Generators.Names.SpaceStations
  ( planetaryStationNameM
  , planetaryStationNames
  , spaceStationNameM
  , spaceStationNames
  )
  where

import Import
import Control.Monad.Random.Strict
import Model

spaceStationNameM :: RandomGen g => StarSystemName -> SpaceStationType -> Rand g SpaceStationName
spaceStationNameM sn st = do
  selector <- chooseM $ makeFrequency (spaceStationNamePatterns st $ _unStarSystemName sn)
  (fromMaybe (adjectiveNoun (adjectiveDistribution st) (nounDistribution st)) selector)


spaceStationNames :: RandomGen g => StarSystemName -> SpaceStationType -> g -> [SpaceStationName]
spaceStationNames s st g =
    n : spaceStationNames s st g'
  where
    (n, g') = runRand (spaceStationNameM s st) g


planetaryStationNameM :: RandomGen g => PlanetName -> PlanetType -> SpaceStationType -> Rand g SpaceStationName
planetaryStationNameM pn pt st = do
  selector <- chooseM $ makeFrequency (planetaryStationNamePatterns pn pt st)
  (fromMaybe (adjectiveNoun (adjectiveDistribution st) (nounDistribution st)) selector)


planetaryStationNames :: RandomGen g => PlanetName -> PlanetType -> SpaceStationType -> g -> [SpaceStationName]
planetaryStationNames pn pt st g =
    n : planetaryStationNames pn pt st g'
  where
    (n, g') = runRand (planetaryStationNameM pn pt st) g


spaceStationNamePatterns :: RandomGen g => SpaceStationType -> Text -> [Rand g SpaceStationName]
spaceStationNamePatterns st place =
  [ adjectiveNoun (adjectiveDistribution st) 
                  (nounDistribution st)
  , adjectiveNounPlace (adjectiveDistribution st) 
                       (nounDistribution st) 
                       place
  ]


planetaryStationNamePatterns :: RandomGen g => PlanetName -> PlanetType -> SpaceStationType -> [Rand g SpaceStationName]
planetaryStationNamePatterns pn pt st =
  [ adjectiveNoun (adjectiveDistribution st) 
                  (nounDistribution st)
  , adjectiveNounPlace (adjectiveDistribution st) 
                       (nounDistribution st) 
                       (_unPlanetName pn)
  , adjectiveNoun (planetaryAdjectiveDistribution pt) 
                  (nounDistribution st)
  , adjectiveNounPlace (planetaryAdjectiveDistribution pt) 
                       (nounDistribution st) 
                       (_unPlanetName pn)
  ]


adjectiveNoun :: RandomGen g => [Frequency Text] -> [Frequency Text] -> Rand g SpaceStationName
adjectiveNoun adjs nouns = do
  adjM <- chooseM adjs
  nounM <- chooseM nouns
  let
    adj = fromMaybe "Unassuming" adjM
    noun = fromMaybe "Station" nounM

  return $ MkSpaceStationName $ adj <> " " <> noun


adjectiveNounPlace :: RandomGen g => [Frequency Text] -> [Frequency Text] -> Text -> Rand g SpaceStationName
adjectiveNounPlace adj noun place = do
  (MkSpaceStationName p1) <- adjectiveNoun adj noun
  return $ MkSpaceStationName $ p1 <> " of " <> place


makeFrequency :: [a] -> [Frequency a]
makeFrequency xs =
  map (Frequency 10) xs


adjectiveDistribution :: SpaceStationType -> [Frequency Text]
adjectiveDistribution MiningStation =
  makeFrequency [ "Excavating", "Golden" ]

adjectiveDistribution ResearchStation =
  makeFrequency [ "Far reaching", "Diligent", "Curious" ]

adjectiveDistribution TradingPost =
  makeFrequency [ "Golden", "Silvery", "Glittering", "Shining" ]

adjectiveDistribution MilitaryBase =
  makeFrequency [ "Swift", "Unwavering", "Brave", "Defending" ]


nounDistribution :: SpaceStationType -> [Frequency Text]
nounDistribution MiningStation =
  makeFrequency [ "Miner", "Extractor", "Drill", "Extraction", "Platform", "Rig" ]

nounDistribution ResearchStation =
  makeFrequency [ "Laboratory", "Research complex", "Observation", "Metering", "Measurement" ]

nounDistribution TradingPost =
  makeFrequency [ "Saucer", "Cloud 9", "Sector 7", "River", "Stream", "Opportunity"
                , "Purse", "Hog", "Sparrow"
                ]

nounDistribution MilitaryBase =
  makeFrequency [ "Bulwark", "Cause", "Shield", "Armour", "Sword", "Axe", "Fortress"
                , "Castle", "Tower", "Outpost", "Station"
                ]


planetaryAdjectiveDistribution :: PlanetType -> [Frequency Text]
planetaryAdjectiveDistribution Geothermal =
  makeFrequency ["Hot", "Molten", "Glowing"]

planetaryAdjectiveDistribution Geomorteus =
  makeFrequency ["Hot", "Scorching"]

planetaryAdjectiveDistribution Geoinactive =
  makeFrequency ["Cool", "Frozen", "Glimmering", "Icy"]

planetaryAdjectiveDistribution Asteroid =
  makeFrequency ["Forlorn", "Last", "Final"]

planetaryAdjectiveDistribution Geoplastic =
  makeFrequency ["Flowing", "Molten"]

planetaryAdjectiveDistribution Geometallic =
  makeFrequency ["Bursting", "Erupting", "Flowing"]

planetaryAdjectiveDistribution Geocrystalline =
  makeFrequency ["Glittering", "Spiky", "Pointed"]

planetaryAdjectiveDistribution Desert =
  makeFrequency ["Parched", "Hot", "Barren"]

planetaryAdjectiveDistribution GasSuperGiant =
  makeFrequency ["Cloudy", "High", "Soaring", "Distant", "Majestetic"]

planetaryAdjectiveDistribution GasGiant =
  makeFrequency ["Cloudy", "High", "Soaring", "Distant"]

planetaryAdjectiveDistribution Adaptable =
  makeFrequency ["Barren", "Promising", "Hopeful", "Ever-improving"]

planetaryAdjectiveDistribution Marginal =
  makeFrequency ["Dusty", "Barren", "Rocky", "Parched"]

planetaryAdjectiveDistribution Terrestrial =
  makeFrequency ["Beautiful", "Hopeful", "Calm", "Peaceful"]

planetaryAdjectiveDistribution Reducing =
  makeFrequency ["Smothering", "Tropical", "Boiling", "Hidden"]

planetaryAdjectiveDistribution Pelagic =
  makeFrequency ["Flowing", "Watery", "Deep", "Misty", "Stormy", "Salty"]

planetaryAdjectiveDistribution Glaciated =
  makeFrequency ["Cold", "Frozen"]

planetaryAdjectiveDistribution Variable =
  makeFrequency ["Changing", "Temporary", "Fleeting"]

planetaryAdjectiveDistribution Rogue =
  makeFrequency ["Forlorn", "Forgotten", "Lost", "Final", "Distant"]

planetaryAdjectiveDistribution Ultragiant =
  makeFrequency ["Tenuous", "Cloudy", "Soaring", "Distant"]

planetaryAdjectiveDistribution Demon =
  makeFrequency ["Poisonous", "Noxious", "Venomous", "Dark", "Shadowy", "Hidden"]
