{-|
Module      : Colours
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module Colours 
  ( background
  , black
  , lightGrey
  , transparent
  , white
  )
  where

import SDL
import Data.Word (Word8)

background :: V4 Word8
background = V4 132 126 135 255

black :: V4 Word8
black = V4 0 0 0 255

white :: V4 Word8
white = V4 255 255 255 255

lightGrey :: V4 Word8
lightGrey = V4 200 200 200 255

transparent :: V4 Word8
transparent = V4 0 0 0 0