{-|
Module      : Graphics
Description : 
Copyright   : (c) Tuula Turto, 2023
License     : GPL-3
Maintainer  : tuula.turto@oktaeder.net
Stability   : experimental
-}
module Graphics
  ( BState(..)
  , createButtonTexture
  , createDialogTexture
  , createInsetTexture
  , createTextSurface
  , createText
  , renderBackground
  , resetViewport
  , setViewport
  ) where

import Foreign.C.Types (CInt)
import qualified RIO.Text as T
import Data.List (maximum)
import Import
import SDL
import Colours
import UI.Types
import Components.Types


data BState =
  BStatePressed
  | BStateReleased
  deriving (Eq)


setViewport :: Coordinates -> Size -> RIO App ()
setViewport (Coordinates x y) (Size w h) = do
  o <- asks appOptions
  r <- asks appRenderer

  let
    (Rectangle (P (V2 vx vy)) _) = o ^. optionsViewport

  rendererViewport r $= (Just $ Rectangle (P (V2 (vx + fromIntegral x) (vy + fromIntegral y)))
                                         (V2 (fromIntegral w) (fromIntegral h)))


-- |Reset viewport back to default
resetViewport :: Options -> Renderer -> RIO App ()
resetViewport o r =
  rendererViewport r $= (Just $ o ^. optionsViewport)


-- |Render background into given coordinates
renderBackground :: Renderer -> Coordinates -> Size -> Background -> RIO App ()
renderBackground r (Coordinates x y) (Size w h) (SolidColourBackground v4) = do
  rendererDrawColor r $= v4
  fillRect r (Just (Rectangle (P (V2 (fromIntegral x) (fromIntegral y))) (V2 (fromIntegral w) (fromIntegral h))))

renderBackground r (Coordinates x y) (Size w h) (BackgroundTexture tex) =
  copy r tex Nothing
       (Just (Rectangle (P $ V2 (fromIntegral x) (fromIntegral y))
       (V2 (fromIntegral w) (fromIntegral h))))

renderBackground _ _ _ TransparentBackground =
  return ()


blitCenter :: Surface -> Surface -> BState -> RIO App ()
blitCenter base extra as = do
  let
    dy = case as of
          BStateReleased ->
            0

          BStatePressed ->
            1

  (V2 bw bh) <- surfaceDimensions base
  (V2 xw xh) <- surfaceDimensions extra

  _ <- surfaceBlit extra
                   Nothing
                   base
                   (Just $ P $ V2 (bw `div` 2 - xw `div` 2) (bh `div` 2 - xh `div` 2 + dy))

  return ()


buttonColourToTextColour :: ButtonColour -> TextColour
buttonColourToTextColour bc =
  case bc of
    BlueButton ->
      BlueText

    RedButton ->
      RedText

    GreenButton ->
      GreenText

    GreyButton ->
      GreyText


-- |Create texture for button
createButtonTexture :: Renderer -> Textures -> Maybe Text -> Maybe Symbol -> ButtonColour -> Size -> RIO App ButtonTexture
createButtonTexture renderer t (Just txt) Nothing c s = do
  pSurface <- createBaseButton t c s BStatePressed Enabled
  pText <- createTextSurface t (buttonColourToTextColour c) Enabled txt
  blitCenter pSurface pText BStatePressed
  _ <- free pText
  pressed <- createTextureFromSurface renderer pSurface
  _ <- free pSurface

  rSurface <- createBaseButton t c s BStateReleased Enabled
  rText <- createTextSurface t (buttonColourToTextColour c) Enabled txt
  blitCenter rSurface rText BStateReleased
  _ <- free rText  
  released <- createTextureFromSurface renderer rSurface
  _ <- free rSurface

  dSurface <- createBaseButton t c s BStateReleased Disabled
  dText <- createTextSurface t (buttonColourToTextColour c) Disabled txt
  blitCenter dSurface dText BStateReleased
  _ <- free dText
  disabled <- createTextureFromSurface renderer dSurface
  _ <- free dSurface

  return $ ButtonTexture
            { _buttonTextureEnabledUp    = released
            , _buttonTextureEnabledDown  = pressed
            , _buttonTextureDisabled     = disabled
            }

createButtonTexture renderer t Nothing (Just symbol) c s = do
  pSurface <- createBaseButton t c s BStatePressed Enabled
  let pSymbol = createSymbolSurface t (buttonColourToSymbolColour c) Enabled symbol
  blitCenter pSurface pSymbol BStatePressed
  pressed <- createTextureFromSurface renderer pSurface
  _ <- free pSurface

  rSurface <- createBaseButton t c s BStateReleased Enabled
  let rSymbol = createSymbolSurface t (buttonColourToSymbolColour c) Enabled symbol
  blitCenter rSurface rSymbol BStateReleased
  released <- createTextureFromSurface renderer rSurface
  _ <- free rSurface

  dSurface <- createBaseButton t c s BStateReleased Disabled
  let dSymbol = createSymbolSurface t (buttonColourToSymbolColour c) Disabled symbol
  blitCenter dSurface dSymbol BStateReleased
  disabled <- createTextureFromSurface renderer dSurface
  _ <- free dSurface

  return $ ButtonTexture
            { _buttonTextureEnabledUp    = released
            , _buttonTextureEnabledDown  = pressed
            , _buttonTextureDisabled     = disabled
            }

createButtonTexture renderer t _ _ c s = do
  pSurface <- createBaseButton t c s BStatePressed Enabled
  pressed <- createTextureFromSurface renderer pSurface
  free pSurface

  rSurface <- createBaseButton t c s BStateReleased Enabled
  released <- createTextureFromSurface renderer rSurface
  free rSurface

  return $ ButtonTexture
            { _buttonTextureEnabledUp    = released
            , _buttonTextureEnabledDown  = pressed
            , _buttonTextureDisabled     = released
            }


-- |Create a single button surface with given colour and size. The button is surrounded
-- with a bevel.
createBaseButton :: Textures -> ButtonColour -> Size -> BState -> ActiveState -> RIO App Surface
createBaseButton t c s@(Size w h) bs Enabled = do
  let
    template = colourToTemplate t c
    base = case bs of
                BStateReleased ->
                    template ^. buttonTemplateDefault

                BStatePressed ->
                    template ^. buttonTemplateActive 
  btn <- assembleBaseButton base $ Size (w - 2) (h - 2)
  bvl <- assembleBevel (t ^. texturesBevel) s
  _ <- surfaceBlit btn Nothing bvl (Just $ P $ V2 1 1)
  free btn

  return bvl

createBaseButton t _ s@(Size w h) _ Disabled = do
  let
    base = t ^. texturesDisabledButton . disabledButtonTemplateButton
  btn <- assembleBaseButton base $ Size (w - 2) (h - 2)
  bvl <- assembleBevel (t ^. texturesBevel) s
  _ <- surfaceBlit btn Nothing bvl (Just $ P $ V2 1 1)
  free btn

  return bvl


-- |Create bevel of given size that has a transparent center
assembleBevel :: Surface -> Size -> RIO App Surface
assembleBevel base (Size w h) = do
  surface <- createRGBSurface (V2 (fromIntegral w) (fromIntegral h)) RGBA8888
  _ <- surfaceFillRect surface Nothing Colours.transparent

  -- corners
  _ <- surfaceBlit base
                    (Just (Rectangle (P $ V2 0 0) (V2 2 2)))
                    surface
                    (Just $ P $ V2 0 0)

  _ <- surfaceBlit base
                    (Just (Rectangle (P $ V2 14 0) (V2 2 2)))
                    surface
                    (Just $ P $ V2 (fromIntegral (w - 2)) 0)

  _ <- surfaceBlit base
                    (Just (Rectangle (P $ V2 0 14) (V2 2 2)))
                    surface
                    (Just $ P $ V2 0 (fromIntegral (h - 2)))

  _ <- surfaceBlit base
                    (Just (Rectangle (P $ V2 14 14) (V2 2 2)))
                    surface
                    (Just $ P $ V2 (fromIntegral (w - 2)) (fromIntegral (h - 2)))
  -- edges
  _ <- surfaceBlitScaled base
                          (Just (Rectangle (P $ V2 2 0) (V2 1 2)))
                          surface
                          (Just (Rectangle (P $ V2 2 0) (V2 (fromIntegral (w - 4)) 2)))

  _ <- surfaceBlitScaled base
                          (Just (Rectangle (P $ V2 14 2) (V2 2 1)))
                          surface
                          (Just (Rectangle (P $ V2 (fromIntegral (w - 2)) 2) (V2 2 (fromIntegral (h - 4)))))

  _ <- surfaceBlitScaled base
                          (Just (Rectangle (P $ V2 2 14) (V2 1 2)))
                          surface
                          (Just (Rectangle (P $ V2 2 (fromIntegral (h - 2))) (V2 (fromIntegral (w - 4)) 2)))

  _ <- surfaceBlitScaled base
                          (Just (Rectangle (P $ V2 0 2) (V2 2 1)))
                          surface
                          (Just (Rectangle (P $ V2 0 2) (V2 2 (fromIntegral (h - 4)))))

  -- middle
  _ <- surfaceBlitScaled base
                          (Just (Rectangle (P $ V2 2 2) (V2 10 10)))
                          surface
                          (Just (Rectangle (P $ V2 2 2) (V2 (fromIntegral (w - 4)) (fromIntegral (h - 4)))))

  return surface  


-- |Create a button of given size using a given surface template
assembleBaseButton :: Surface -> Size -> RIO App Surface
assembleBaseButton base (Size w h) = do
  surface <- createRGBSurface (V2 (fromIntegral w) (fromIntegral h)) RGBA8888
  _ <- surfaceFillRect surface Nothing Colours.transparent

  -- corners
  _ <- surfaceBlit base
                    (Just (Rectangle (P $ V2 0 0) (V2 2 2)))
                    surface
                    (Just $ P $ V2 0 0)

  _ <- surfaceBlit base
                    (Just (Rectangle (P $ V2 12 0) (V2 2 2)))
                    surface
                    (Just $ P $ V2 (fromIntegral (w - 2)) 0)

  _ <- surfaceBlit base
                    (Just (Rectangle (P $ V2 0 12) (V2 2 2)))
                    surface
                    (Just $ P $ V2 0 (fromIntegral (h - 2)))

  _ <- surfaceBlit base
                    (Just (Rectangle (P $ V2 12 12) (V2 2 2)))
                    surface
                    (Just $ P $ V2 (fromIntegral (w - 2)) (fromIntegral (h - 2)))
  -- edges
  _ <- surfaceBlitScaled base
                          (Just (Rectangle (P $ V2 2 0) (V2 1 2)))
                          surface
                          (Just (Rectangle (P $ V2 2 0) (V2 (fromIntegral (w - 4)) 2)))

  _ <- surfaceBlitScaled base
                          (Just (Rectangle (P $ V2 12 2) (V2 2 1)))
                          surface
                          (Just (Rectangle (P $ V2 (fromIntegral (w - 2)) 2) (V2 2 (fromIntegral (h - 4)))))

  _ <- surfaceBlitScaled base
                          (Just (Rectangle (P $ V2 2 12) (V2 1 2)))
                          surface
                          (Just (Rectangle (P $ V2 2 (fromIntegral (h - 2))) (V2 (fromIntegral (w - 4)) 2)))

  _ <- surfaceBlitScaled base
                          (Just (Rectangle (P $ V2 0 2) (V2 2 1)))
                          surface
                          (Just (Rectangle (P $ V2 0 2) (V2 2 (fromIntegral (h - 4)))))

  -- middle
  _ <- surfaceBlitScaled base
                          (Just (Rectangle (P $ V2 2 2) (V2 10 10)))
                          surface
                          (Just (Rectangle (P $ V2 2 2) (V2 (fromIntegral (w - 4)) (fromIntegral (h - 4)))))

  return surface  


-- |Get correct button template based on button colour
colourToTemplate :: Textures -> ButtonColour -> ButtonTemplate
colourToTemplate t c =
  case c of
    RedButton ->
      _texturesRedButton t

    BlueButton ->
      _texturesBlueButton t
                
    GreenButton ->
      _texturesGreenButton t

    GreyButton ->
      _texturesGreyButton t


buttonColourToSymbolColour :: ButtonColour -> SymbolColour
buttonColourToSymbolColour c =
  case c of
    RedButton ->
      RedSymbol

    BlueButton ->
      BlueSymbol

    GreenButton ->
      GreenSymbol

    GreyButton ->
      GreySymbol


createSymbolSurface :: Textures -> SymbolColour -> ActiveState -> Symbol -> Surface
createSymbolSurface t sc as s =
  let 
    template = case as of
                    Enabled ->
                      case sc of
                        RedSymbol ->
                          t ^. texturesRedButton . buttonTemplateSymbols

                        BlueSymbol ->
                          t ^. texturesBlueButton . buttonTemplateSymbols

                        GreenSymbol ->
                          t ^. texturesGreenButton . buttonTemplateSymbols

                        GreySymbol ->
                          t ^. texturesGreyButton . buttonTemplateSymbols

                    Disabled ->
                      t ^. texturesDisabledButton . disabledButtonTemplateSymbols

  in
    case s of
      SymbolPlus ->
        template ^. buttonSymbolsPlus

      SymbolMinus ->
        template ^. buttonSymbolsMinus

      SymbolLeftArrow ->
        template ^. buttonSymbolsLeftArrow

      SymbolRightArrow ->
        template ^. buttonSymbolsRightArrow

      SymbolUpArrow ->
        template ^. buttonSymbolsUpArrow

      SymbolDownArrow ->
        template ^. buttonSymbolsDownArrow

      SymbolCWArrow ->
        template ^. buttonSymbolsCWArrow

      SymbolCCWArrow ->
        template ^. buttonSymbolsCWWArrow

      SymbolX ->
        template ^. buttonSymbolsX


-- |Create texture for a dialog.
createDialogTexture :: Renderer -> Textures -> Size -> RIO App Texture
createDialogTexture renderer t (Size w h) = do
  let
    base = t ^. texturesRidge

  surface <- createRGBSurface (V2 (fromIntegral w) (fromIntegral h)) RGBA8888
  _ <- surfaceFillRect surface Nothing Colours.transparent

  -- corners
  _ <- surfaceBlit base
                    (Just (Rectangle (P $ V2 0 0) (V2 6 6)))
                    surface
                    (Just $ P $ V2 0 0)

  _ <- surfaceBlit base
                    (Just (Rectangle (P $ V2 26 0) (V2 6 6)))
                    surface
                    (Just $ P $ V2 (fromIntegral (w - 6)) 0)

  _ <- surfaceBlit base
                    (Just (Rectangle (P $ V2 0 26) (V2 6 6)))
                    surface
                    (Just $ P $ V2 0 (fromIntegral (h - 6)))

  _ <- surfaceBlit base
                    (Just (Rectangle (P $ V2 26 26) (V2 6 6)))
                    surface
                    (Just $ P $ V2 (fromIntegral (w - 6)) (fromIntegral (h - 6)))
  -- edges
  _ <- surfaceBlitScaled base
                          (Just (Rectangle (P $ V2 6 0) (V2 1 6)))
                          surface
                          (Just (Rectangle (P $ V2 6 0) (V2 (fromIntegral (w - 12)) 6)))

  _ <- surfaceBlitScaled base
                          (Just (Rectangle (P $ V2 26 6) (V2 6 1)))
                          surface
                          (Just (Rectangle (P $ V2 (fromIntegral (w - 6)) 6) (V2 6 (fromIntegral (h - 12)))))

  _ <- surfaceBlitScaled base
                          (Just (Rectangle (P $ V2 6 26) (V2 1 6)))
                          surface
                          (Just (Rectangle (P $ V2 6 (fromIntegral (h - 6))) (V2 (fromIntegral (w - 12)) 6)))

  _ <- surfaceBlitScaled base
                          (Just (Rectangle (P $ V2 0 6) (V2 6 1)))
                          surface
                          (Just (Rectangle (P $ V2 0 6) (V2 6 (fromIntegral (h - 12)))))

  -- middle
  _ <- surfaceBlitScaled base
                          (Just (Rectangle (P $ V2 8 8) (V2 16 16)))
                          surface
                          (Just (Rectangle (P $ V2 6 6) (V2 (fromIntegral (w - 12)) (fromIntegral (h - 12)))))

  txt <- createTextureFromSurface renderer surface
  free surface
  
  return txt


createText :: Renderer -> Textures -> TextColour -> ActiveState -> Size -> Text -> RIO App Texture
createText renderer t tc as (Size w h) txt = do
  surface <- createTextSurface t tc as txt
  sizedSurface <- createRGBSurface (V2 (fromIntegral w) (fromIntegral h)) RGBA8888
  _ <- surfaceBlit surface
                   Nothing
                   sizedSurface
                   (Just $ P $ V2 0 0)

  texture <- createTextureFromSurface renderer sizedSurface
  free surface
  free sizedSurface
  return texture


createTextSurface :: Textures -> TextColour -> ActiveState -> Text -> RIO App Surface
createTextSurface t tc as txt = do
  let
    txts = lines (T.unpack . T.toLower $ txt)
    font = case as of
            Enabled ->
              textColourToSurface t tc

            Disabled ->
              t ^. texturesDisabledText

  lineSurfaces <- mapM (createLine font) txts
  dimensions <- mapM surfaceDimensions lineSurfaces
  
  let
    h = 8 * length lineSurfaces
    w = maximum $ map (\(V2 ws _) -> ws) dimensions
    pairs = zip [0..] lineSurfaces

  surface <- createRGBSurface (V2 w (fromIntegral h)) RGBA8888

  -- blit lines on surface
  mapM_ (\(i, line) -> surfaceBlit line
                                   Nothing
                                   surface
                                   (Just $ P $ V2 0 (i * 8)))
        pairs

  return surface


textColourToSurface :: Textures -> TextColour -> Surface
textColourToSurface t tc =
  case tc of
    RedText ->
      t ^. texturesRedText
    
    BlueText ->
      t ^. texturesBlueText

    GreenText ->
      t ^. texturesGreenText

    GreyText ->
      t ^. texturesGreyText

    

createLine :: Surface -> String -> RIO App Surface
createLine font txt = do
  surface <- createRGBSurface (V2 (fromIntegral (8 * length txt)) 7) RGBA8888
  w <- foldM (addCharacter font surface) 0 txt
  fSurface <- createRGBSurface (V2 w 7) RGBA8888
  _ <- surfaceBlit surface Nothing fSurface (Just $ P $ V2 0 0)
  free surface
  return fSurface


addCharacter :: Surface -> Surface -> CInt -> Char -> RIO App CInt
addCharacter font l x c = do
  let
    (sx, sy, CSize w h) = fontMap c

  _ <- surfaceBlit font
                   (Just $ Rectangle (P $ V2 sx sy) (V2 w h))
                   l 
                   (Just $ P $ V2 x 0)

  return $ x + w + 1


data CSize = CSize CInt CInt
  deriving (Show, Read, Eq)
  

fontMap :: Char -> (CInt, CInt, CSize)
fontMap 'a' = (4, 4, CSize 5 7)
fontMap 'b' = (10, 4, CSize 5 7)
fontMap 'c' = (16, 4, CSize 5 7)
fontMap 'd' = (22, 4, CSize 5 7)
fontMap 'e' = (28, 4, CSize 5 7)
fontMap 'f' = (34, 4, CSize 5 7)
fontMap 'g' = (40, 4, CSize 5 7)
fontMap 'h' = (46, 4, CSize 5 7)
fontMap 'i' = (52, 4, CSize 5 7)
fontMap 'j' = (58, 4, CSize 5 7)
fontMap 'k' = (64, 4, CSize 5 7)
fontMap 'l' = (70, 4, CSize 5 7)
fontMap 'm' = (76, 4, CSize 7 7)
fontMap 'n' = (84, 4, CSize 6 7)
fontMap 'o' = (91, 4, CSize 5 7)
fontMap 'p' = (97, 4, CSize 5 7)
fontMap 'q' = (103, 4, CSize 6 7)
fontMap 'r' = (110, 4, CSize 5 7)
fontMap 's' = (116, 4, CSize 5 7)
fontMap 't' = (122, 4, CSize 5 7)
fontMap 'u' = (128, 4, CSize 5 7)
fontMap 'v' = (134, 4, CSize 5 7)
fontMap 'w' = (140, 4, CSize 7 7)
fontMap 'x' = (148, 4, CSize 5 7)
fontMap 'y' = (154, 4, CSize 5 7)
fontMap 'z' = (160, 4, CSize 5 7)
fontMap '0' = (4, 13, CSize 5 7)
fontMap '1' = (10, 13, CSize 5 7)
fontMap '2' = (16, 13, CSize 5 7)
fontMap '3' = (22, 13, CSize 5 7)
fontMap '4' = (28, 13, CSize 5 7)
fontMap '5' = (34, 13, CSize 5 7)
fontMap '6' = (40, 13, CSize 5 7)
fontMap '7' = (46, 13, CSize 5 7)
fontMap '8' = (52, 13, CSize 5 7)
fontMap '9' = (58, 13, CSize 5 7)
fontMap '?' = (70, 13, CSize 5 7)
fontMap _   = (64, 13, CSize 5 7)


-- |Create texture for an inset of given size
createInsetTexture :: Renderer -> Textures -> Size -> RIO App Texture
createInsetTexture renderer t (Size w h) = do
  if (w < 16 || h < 16)
  then
    logWarn $ displayShow $ "Tried to create too small inset: " ++ (show w) ++ "x" ++ (show h)
  else
    return ()

  surface <- createRGBSurface (V2 (fromIntegral w) (fromIntegral h)) RGBA8888
  createInsetCorners t surface
  createInsetEdges t surface

  _ <- surfaceBlitScaled (t ^. texturesInset)
            (Just $ Rectangle (P $ V2 8 8) (V2 16 16))
            surface
            (Just $ Rectangle (P $ V2 8 8) (V2 ((fromIntegral w) - 16) ((fromIntegral h) - 16)))

  texture <- createTextureFromSurface renderer surface
  free surface

  return texture
  

-- |Render corners of an inset on a given surface
createInsetCorners :: Textures -> Surface -> RIO App ()
createInsetCorners t destination = do
  let 
    source = t ^. texturesInset
  V2 w h <- surfaceDimensions destination
  _ <- surfaceBlit source
                   (Just $ Rectangle (P $ V2 0 0) (V2 8 8))
                   destination
                   (Just $ P $ V2 0 0)  
  _ <- surfaceBlit source
                  (Just $ Rectangle (P $ V2 24 0) (V2 8 8))
                  destination
                  (Just $ P $ V2 (w - 8) 0)
  _ <- surfaceBlit source
                  (Just $ Rectangle (P $ V2 0 24) (V2 8 8))
                  destination
                  (Just $ P $ V2 0 (h - 8))
  _ <- surfaceBlit source
                  (Just $ Rectangle (P $ V2 24 24) (V2 8 8))
                  destination
                  (Just $ P $ V2 (w - 8) (h - 8))
  return ()

-- |Render edges of an inset on a given surface
createInsetEdges :: Textures -> Surface -> RIO App ()
createInsetEdges t destination = do
  let 
    source = t ^. texturesInset
  V2 w h <- surfaceDimensions destination
  addHorizonalEdge source destination 8 (w - 8)
  addVerticalEdge source destination 8 (h - 8)
  return ()


addHorizonalEdge :: Surface -> Surface -> CInt -> CInt -> RIO App ()
addHorizonalEdge source destination start end = do
  V2 _ h <- surfaceDimensions destination
  if start > (end - 16)
    then do
      _ <- surfaceBlit source
                (Just $ Rectangle (P $ V2 8 0) (V2 (end - start) 8))
                destination
                (Just $ P $ V2 start 0)
      _ <- surfaceBlit source
                (Just $ Rectangle (P $ V2 8 24) (V2 (end - start) 8))
                destination
                (Just $ P $ V2 start (h - 8))
      return ()
    else do
      _ <- surfaceBlit source
                (Just $ Rectangle (P $ V2 8 0) (V2 16 8))
                destination
                (Just $ P $ V2 start 0)
      _ <- surfaceBlit source
                (Just $ Rectangle (P $ V2 8 24) (V2 16 8))
                destination
                (Just $ P $ V2 start (h - 8))
      addHorizonalEdge source destination (start + 16) end

  return ()


addVerticalEdge :: Surface -> Surface -> CInt -> CInt -> RIO App ()
addVerticalEdge source destination start end = do
  V2 w _ <- surfaceDimensions destination
  if start > (end - 16)
    then do
      _ <- surfaceBlit source
                (Just $ Rectangle (P $ V2 0 8) (V2 16 (end - start)))
                destination
                (Just $ P $ V2 0 start)
      _ <- surfaceBlit source
                (Just $ Rectangle (P $ V2 24 8) (V2 8 (end - start)))
                destination
                (Just $ P $ V2 (w - 8) start)
      return ()
    else do
      _ <- surfaceBlit source
                (Just $ Rectangle (P $ V2 0 8) (V2 8 16))
                destination
                (Just $ P $ V2 0 start)
      _ <- surfaceBlit source
                (Just $ Rectangle (P $ V2 24 8) (V2 8 16))
                destination
                (Just $ P $ V2 (w - 8) start)
      addVerticalEdge source destination (start + 16) end

  return ()
